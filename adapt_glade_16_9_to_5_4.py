


xml_file = open("futbolin_16_9.glade", "r")

output_file = open("futbolin_5_4.glade", "w")

# To only change the first three 720 for 1024
change_count = 3

next_is_mundialito_button = False

for line in xml_file:

    # Modify the mundialito buttons lines to keep them in the corners of the 5:4 screen

    if '<object class="GtkEventBox" id="ebox_b_mundialito_' in line:
        next_is_mundialito_button = True

    elif next_is_mundialito_button and '<property name="width_request">100</property>' in line:
        line = line.replace('100', '250')

    elif next_is_mundialito_button and '<property name="height_request">' in line:
        line = line.replace('67', '219')
        line = line.replace('42', '194')
        line = line.replace('61', '213')

    elif next_is_mundialito_button and 'name="x"' in line:
        old_x = line.strip()[19:-11]

        if old_x == '1180':
            new_x = '1030'
            line = line.replace(old_x, new_x)

    elif next_is_mundialito_button and 'name="y"' in line:
        old_y = line.strip()[19:-11]

        if old_y == '678':
            new_y = '830'
            line = line.replace(old_y, new_y)

        elif old_y == '659':
            new_y = '811'
            line = line.replace(old_y, new_y)

        next_is_mundialito_button = False

    elif 'name="y"' in line:        
        old_y = line.strip()[19:-11]
        new_y = str(int(old_y) + 152)
        line = line.replace(old_y, new_y)

    elif change_count > 0 and '"height_request">720' in line:
        line = line.replace('720', '1024')
        change_count -= 1

    elif change_count > 0 and '"height">720' in line:        
        line = line.replace('720', '1024')
        change_count -= 1

    elif '<property name="pixbuf">images/background.jpg</property>' in line:
        line = line.replace("background.jpg", "background_5_4.jpg")    
                
    output_file.write(line)




xml_file.close()
output_file.close()

print "End."
