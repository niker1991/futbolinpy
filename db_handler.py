'''
Created on 23/4/2015

@author: Nicanor Romero Venier
'''

import sqlite3
import codecs
import sys
import platform

from datetime import date


" *** Classes *** "

class Player:
    def __init__(self, pid, name, surname, nickname, active, n_matches=0, play_time=0, mad_wins=0, mad_losses=0, atl_wins=0, atl_losses=0,
                 def_wins=0, def_losses=0, atk_wins=0, atk_losses=0, n_goals=0, n_autogoals=0, n_rcvd_goals=0, n_rcvd_goals_helper=0):
        self.pid = pid
        self.name = name
        self.surname = surname
        self.nickname = nickname
        self.active = active
        self.n_matches = n_matches
        self.play_time = play_time
        self.mad_wins = mad_wins
        self.mad_losses = mad_losses
        self.atl_wins = atl_wins
        self.atl_losses = atl_losses
        self.def_wins = def_wins
        self.def_losses = def_losses
        self.atk_wins = atk_wins
        self.atk_losses = atk_losses
        self.n_goals = n_goals
        self.n_autogoals = n_autogoals
        self.n_rcvd_goals = n_rcvd_goals
        self.n_rcvd_goals_helper = n_rcvd_goals_helper

class Match:
    def __init__(self, mid, mad_def_pid, mad_atk_pid, atl_def_pid, atl_atk_pid, score_mad, score_atl, date, start_time, match_time):
        self.mid = mid
        self.mad_def_pid = mad_def_pid
        self.mad_atk_pid = mad_atk_pid
        self.atl_def_pid = atl_def_pid
        self.atl_atk_pid = atl_atk_pid
        self.score_mad = score_mad
        self.score_atl = score_atl
        self.date = date
        self.start_time = start_time
        self.match_time = match_time

class Season:
    def __init__(self, sid, season_n, initial_mid, final_mid):
        self.sid = sid
        self.season_n = season_n
        self.initial_mid = initial_mid
        self.final_mid = final_mid 
    
class Goal:
    def __init__(self, mid, pid, goal_time, figure, legal):
        self.mid = mid
        self.pid = pid
        self.goal_time = goal_time
        self.figure = figure
        self.legal = legal

class Championship:
    def __init__(self, cid, match_1, match_2, match_3, match_4, match_5, match_6, match_7, winner, runner_up, finished):
        
        self.cid = cid
        self.teams = []
        
        for match in [match_1, match_2, match_3, match_4]:
            self.teams.append([int(pid) for pid in match.split(',')[0:2]])
            self.teams.append([int(pid) for pid in match.split(',')[2:4]])
        
        self.matches = [[int(match_data) for match_data in match.split(',')] for match in [match_1, match_2, match_3, match_4, match_5, match_6, match_7]]
        self.winner = [str(e) for e in winner.split(',')]
        self.runner_up = [str(e) for e in runner_up.split(',')]
        self.finished = finished

" *** Create SQL Tables *** "

def create_players_table():
    
    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()
        
    c.execute("CREATE TABLE IF NOT EXISTS Players (pid                 INTEGER PRIMARY KEY,\
                                                   name                TEXT NOT NULL,\
                                                   surname             TEXT NOT NULL,\
                                                   nickname            TEXT NOT NULL,\
                                                   active              INTEGER DEFAULT 1,\
                                                   n_matches           INTEGER DEFAULT 0,\
                                                   play_time           INTEGER DEFAULT 0,\
                                                   mad_wins            INTEGER DEFAULT 0,\
                                                   mad_losses          INTEGER DEFAULT 0,\
                                                   atl_wins            INTEGER DEFAULT 0,\
                                                   atl_losses          INTEGER DEFAULT 0,\
                                                   def_wins            INTEGER DEFAULT 0,\
                                                   def_losses          INTEGER DEFAULT 0,\
                                                   atk_wins            INTEGER DEFAULT 0,\
                                                   atk_losses          INTEGER DEFAULT 0,\
                                                   n_goals             INTEGER DEFAULT 0,\
                                                   n_autogoals         INTEGER DEFAULT 0,\
                                                   n_rcvd_goals        INTEGER DEFAULT 0,\
                                                   n_rcvd_goals_helper INTEGER DEFAULT 0);")    
    
    conn.commit()
    conn.close()
    
    return

def create_matches_table():
    
    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()
        
    c.execute("CREATE TABLE IF NOT EXISTS Matches (mid         INTEGER PRIMARY KEY,\
                                                   mad_def_pid INTEGER,\
                                                   mad_atk_pid INTEGER,\
                                                   atl_def_pid INTEGER,\
                                                   atl_atk_pid INTEGER,\
                                                   score_mad   INTEGER,\
                                                   score_atl   INTEGER,\
                                                   date        TEXT,\
                                                   start_time  TEXT,\
                                                   match_time  INTEGER,\
                                                   FOREIGN KEY (mad_def_pid) REFERENCES Player(pid),\
                                                   FOREIGN KEY (mad_atk_pid) REFERENCES Player(pid),\
                                                   FOREIGN KEY (atl_def_pid) REFERENCES Player(pid),\
                                                   FOREIGN KEY (atl_atk_pid) REFERENCES Player(pid));")
    
    conn.commit()
    conn.close()
    
    return

def create_seasons_table():
    
    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()
        
    c.execute("CREATE TABLE IF NOT EXISTS Seasons (sid         INTEGER PRIMARY KEY,\
                                                   season_n    INTEGER,\
                                                   initial_mid INTEGER,\
                                                   final_mid   INTEGER);")
    
    conn.commit()
    conn.close()
    
    return

def create_goals_table():
    
    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()
        
    c.execute("CREATE TABLE IF NOT EXISTS Goals (mid       INTEGER,\
                                                 pid       INTEGER,\
                                                 goal_time INTEGER,\
                                                 figure    INTEGER,\
                                                 legal     INTEGER,\
                                                 FOREIGN KEY (mid) REFERENCES Matches(mid),\
                                                 FOREIGN KEY (pid) REFERENCES Players(pid));")
    
    conn.commit()
    conn.close()
    
    return

def create_championships_table():
    
    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()
        
    c.execute("CREATE TABLE IF NOT EXISTS Championships (cid       INTEGER PRIMARY KEY,\
                                                         match_1   TEXT,\
                                                         match_2   TEXT,\
                                                         match_3   TEXT,\
                                                         match_4   TEXT,\
                                                         match_5   TEXT DEFAULT '-1, -1, -1, -1, -1, -1',\
                                                         match_6   TEXT DEFAULT '-1, -1, -1, -1, -1, -1',\
                                                         match_7   TEXT DEFAULT '-1, -1, -1, -1, -1, -1',\
                                                         winner    TEXT DEFAULT '-1, -1',\
                                                         runner_up TEXT DEFAULT '-1, -1',\
                                                         finished  INTEGER DEFAULT 0);")
    
    conn.commit()
    conn.close()
    
    return


def empty_table(table_name):
    
    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()
    
    c.execute("DELETE FROM %s;" % table_name)
    c.execute("VACUUM")    
    
    conn.commit()
    conn.close()
    
    return


" *** Set and Get functions *** "

def set_player(name, surname, nickname, pid=-1):
    
    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()        
    
    # If pid not provided, create a new PRIMARY KEY in the database
    
    if pid == -1:
        c.execute("INSERT INTO Players (name, surname, nickname) VALUES (?, ?, ?);", (name, surname, nickname))
    else:
        c.execute("INSERT INTO Players (pid, name, surname, nickname) VALUES (?, ?, ?, ?);", (pid, name, surname, nickname))
    
    conn.commit()
    conn.close()
    
    return c.lastrowid

def get_player(pid=-1, nickname=''):
    
    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()
    
    # If pid not provided, search Player by name
    
    if pid != -1:
        c.execute("SELECT * FROM Players WHERE pid=?;", (pid,))
    elif nickname != '':
        c.execute("SELECT * FROM Players WHERE nickname=?;", (nickname,))
        
    player_db = c.fetchone()
    
    if player_db == None: return None
    
    player = Player(*player_db)
    
    conn.close()
      
    return player

def get_n_players():
    
    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()
        
    c.execute("SELECT COUNT(*) FROM Players;")
        
    n_players = c.fetchone()[0]
    
    conn.close()
      
    return n_players

def get_player_nickname(pid):
    
    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()
        
    c.execute("SELECT nickname FROM Players WHERE pid=?;", (pid,))
        
    player_nickname = c.fetchone()[0]
    
    conn.close()
      
    return player_nickname

def get_active_players_pids():
    
    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()
        
    c.execute("SELECT pid FROM Players WHERE active=1;")
        
    players_pids = c.fetchall()
    
    players_pids = [i[0] for i in players_pids]
    
    conn.close()
      
    return players_pids


def set_match(mad_def_pid, mad_atk_pid, atl_def_pid, atl_atk_pid, score_mad, score_atl, date, start_time, match_time, mid=-1):

    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()        
    
    # If mid not provided, create a new PRIMARY KEY in the database
    
    if mid == -1:
        c.execute("INSERT INTO Matches (mad_def_pid, mad_atk_pid, atl_def_pid, atl_atk_pid, score_mad, score_atl, date, start_time, match_time) \
                   VALUES (?, ?, ? ,?, ?, ?, ?, ?, ?);", (mad_def_pid, mad_atk_pid, atl_def_pid, atl_atk_pid, score_mad, score_atl, date, start_time, match_time,))
    else:
        c.execute("INSERT INTO Matches (mid, mad_def_pid, mad_atk_pid, atl_def_pid, atl_atk_pid, score_mad, score_atl, date, start_time, match_time) \
                   VALUES (?, ?, ?, ? ,?, ?, ?, ?, ?, ?);", (mid, mad_def_pid, mad_atk_pid, atl_def_pid, atl_atk_pid, score_mad, score_atl, date, start_time, match_time,))
    
    conn.commit()
    conn.close()
    
    return c.lastrowid

def get_match(mid):
    
    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()
    
    c.execute("SELECT * FROM Matches WHERE mid=?;", (mid,))
    
    match_db = c.fetchone()
    
    if match_db == None: return None
    
    match = Match(*match_db)
    
    conn.close()
      
    return match

def get_last_mid():
    
    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()
    
    c.execute("SELECT mid FROM Matches ORDER BY mid DESC;")
    
    last_mid = c.fetchone()[0]
    
    conn.close()
      
    return last_mid

def get_n_matches_last_season(pid):
    
    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()
    
    # Get last season sid
    
    c.execute("SELECT sid FROM Seasons ORDER BY sid DESC;")
    
    last_sid = c.fetchone()[0]
    
    # Get first and last mid from last season
        
    c.execute("SELECT initial_mid, final_mid FROM Seasons WHERE sid=?;", (last_sid,))
        
    initial_mid, final_mid = c.fetchone()
        
    if pid != -1:
        # Get number of matches of the PID in the last season
            
        c.execute("SELECT COUNT(*) FROM Matches WHERE ((mid >= ? and mid <= ?) and (mad_def_pid=? or mad_atk_pid=? or atl_def_pid=? or atl_atk_pid=?));",
                 (initial_mid, final_mid, pid, pid, pid, pid))
    
    elif pid == -1:
        # Get the total number of matches of the last season
        
        c.execute("SELECT COUNT(*) FROM Matches WHERE (mid >= ? and mid <= ?);", (initial_mid, final_mid,))
        
    n_matches = c.fetchone()[0]
            
    conn.close()
        
    return n_matches

def get_days_since_last_match(pid):
    
    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()
    
    c.execute("SELECT date FROM Matches WHERE (mad_def_pid=? or mad_atk_pid=? or atl_def_pid=? or atl_atk_pid=?) ORDER BY date DESC;", (pid, pid, pid, pid,))
    
    last_match_date = c.fetchone()
    
    if last_match_date == None: return -1
    
    conn.close()
        
    last_match_date = [int(i) for i in last_match_date[0].split('-')]
            
    days_since_last_match = (date.today() - date(*last_match_date))
      
    return days_since_last_match.days

def get_n_matches_between(pid_1, pid_2, pid_3, pid_4):
    
    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()
    
    # Get matches where (pid_1, pid_2) are Madrid and (pid_3, pid_4) are Atletico
    
    c.execute("SELECT COUNT(*) FROM Matches WHERE (mad_def_pid=? and mad_atk_pid=? and atl_def_pid=? and atl_atk_pid=?) OR \
                                                  (mad_def_pid=? and mad_atk_pid=? and atl_def_pid=? and atl_atk_pid=?) OR \
                                                  (mad_def_pid=? and mad_atk_pid=? and atl_def_pid=? and atl_atk_pid=?) OR \
                                                  (mad_def_pid=? and mad_atk_pid=? and atl_def_pid=? and atl_atk_pid=?);", (pid_1, pid_2, pid_3, pid_4,
                                                                                                                            pid_1, pid_2, pid_4, pid_3,
                                                                                                                            pid_2, pid_1, pid_3, pid_4,
                                                                                                                            pid_2, pid_1, pid_4, pid_3))
        
    n_matches = c.fetchone()[0]
        
    # Get matches where (pid_1, pid_2) are Atletico and (pid_3, pid_4) are Madrid
    
    c.execute("SELECT COUNT(*) FROM Matches WHERE (mad_def_pid=? and mad_atk_pid=? and atl_def_pid=? and atl_atk_pid=?) OR \
                                                  (mad_def_pid=? and mad_atk_pid=? and atl_def_pid=? and atl_atk_pid=?) OR \
                                                  (mad_def_pid=? and mad_atk_pid=? and atl_def_pid=? and atl_atk_pid=?) OR \
                                                  (mad_def_pid=? and mad_atk_pid=? and atl_def_pid=? and atl_atk_pid=?);", (pid_3, pid_4, pid_1, pid_2,
                                                                                                                            pid_3, pid_4, pid_2, pid_1,
                                                                                                                            pid_4, pid_3, pid_1, pid_2,
                                                                                                                            pid_4, pid_3, pid_2, pid_1))
    
    n_matches += c.fetchone()[0]
        
    conn.close()
                
    return n_matches

def get_matches_between(pid_1, pid_2, pid_3, pid_4):
    
    # [7-0, 6-1, 5-1, 4-1, 4-2, 4-3, 3-4, 2-4, 1-4, 1-5, 1-6, 0-7]
    
    possible_results = [(7,0), (6,1), (5,1), (4,1), (4,2), (4,3), (3,4), (2,4), (1,4), (1,5), (1,6), (0,7)]
    
    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()
    
    # Get matches where (pid_1, pid_2) are Madrid and (pid_3, pid_4) are Atletico
    
    c.execute("SELECT score_mad, score_atl FROM Matches WHERE (mad_def_pid=? and mad_atk_pid=? and atl_def_pid=? and atl_atk_pid=?) OR \
                                                              (mad_def_pid=? and mad_atk_pid=? and atl_def_pid=? and atl_atk_pid=?) OR \
                                                              (mad_def_pid=? and mad_atk_pid=? and atl_def_pid=? and atl_atk_pid=?) OR \
                                                              (mad_def_pid=? and mad_atk_pid=? and atl_def_pid=? and atl_atk_pid=?);", (pid_1, pid_2, pid_3, pid_4,
                                                                                                                                        pid_1, pid_2, pid_4, pid_3,
                                                                                                                                        pid_2, pid_1, pid_3, pid_4,
                                                                                                                                        pid_2, pid_1, pid_4, pid_3))
    
    matches_mad_atl = c.fetchall()
        
    # Get matches where (pid_1, pid_2) are Atletico and (pid_3, pid_4) are Madrid
    
    c.execute("SELECT score_mad, score_atl FROM Matches WHERE (mad_def_pid=? and mad_atk_pid=? and atl_def_pid=? and atl_atk_pid=?) OR \
                                                              (mad_def_pid=? and mad_atk_pid=? and atl_def_pid=? and atl_atk_pid=?) OR \
                                                              (mad_def_pid=? and mad_atk_pid=? and atl_def_pid=? and atl_atk_pid=?) OR \
                                                              (mad_def_pid=? and mad_atk_pid=? and atl_def_pid=? and atl_atk_pid=?);", (pid_3, pid_4, pid_1, pid_2,
                                                                                                                                        pid_3, pid_4, pid_2, pid_1,
                                                                                                                                        pid_4, pid_3, pid_1, pid_2,
                                                                                                                                        pid_4, pid_3, pid_2, pid_1))
    
    matches_atl_mad = c.fetchall()
        
    conn.close()
    
    matches_between = [0] * 12
    
    # Some matches are stored with not valid results (example: 4-0)
    for result in matches_mad_atl:
        if result in possible_results:
            matches_between[possible_results.index(result)] += 1
            
    for result in matches_atl_mad:
        if result in possible_results:
            matches_between[-1-possible_results.index(result)] += 1
                
    return matches_between

def get_7_0_matches(sid):
    
    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()
        
    # Get the last sid
        
    last_sid = get_last_sid()
        
    # If the sid provided is from current season, use final_mid from last season
    
    if sid > last_sid:
        
        # Get final mid from last season
        
        c.execute("SELECT final_mid FROM Seasons WHERE sid=?;", (last_sid,))
            
        final_mid = c.fetchone()[0]
            
        # Get all 7-0 or 0-7 matches of current season
             
        c.execute("SELECT * FROM Matches WHERE (((score_mad=7 and score_atl=0) or (score_mad=0 and score_atl=7)) and (mid > ?));", (final_mid, ))
    
    else:
        # Get initial and final mid from season
        
        c.execute("SELECT initial_mid, final_mid FROM Seasons WHERE sid=?;", (sid,))
            
        initial_mid, final_mid = c.fetchone()
            
        # Get all 7-0 or 0-7 matches of current season
             
        c.execute("SELECT * FROM Matches WHERE (((score_mad=7 and score_atl=0) or (score_mad=0 and score_atl=7)) and (mid >= ? and mid <= ?));", (initial_mid, final_mid, ))   
    
    matches_list = []
    
    for match_data in c.fetchall():
        matches_list.append(Match(*match_data))
            
    conn.close()
        
    return matches_list

def get_araujo_0_7():
    
    pids_dict = {}
    
    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()
    
    c.execute("SELECT mad_def_pid, mad_atk_pid FROM Matches WHERE ((atl_def_pid=2 OR atl_atk_pid=2) AND score_mad=7);")
    
    pids_list = [couple for i in c.fetchall() for couple in i]
        
    c.execute("SELECT atl_def_pid, atl_atk_pid FROM Matches WHERE ((mad_def_pid=2 OR mad_atk_pid=2) AND score_atl=7);")
    
    pids_list += [couple for i in c.fetchall() for couple in i]
            
    conn.close()
    
    for i in pids_list:
        if pids_dict.has_key(i):
            pids_dict[i] += 1
        else:
            pids_dict[i] = 1
            
    pids_sorted = [(i, pids_dict[i]) for i in sorted(pids_dict, key=pids_dict.get, reverse=True)]
      
    return pids_sorted


def set_season(season_n, initial_mid, final_mid, sid=-1):

    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()        
    
    # If sid not provided, create a new PRIMARY KEY in the database
    
    if sid == -1:
        c.execute("INSERT INTO Seasons (season_n, initial_mid, final_mid) VALUES (?, ?, ?);", (season_n, initial_mid, final_mid,))
    else:
        c.execute("INSERT INTO Seasons (sid, season_n, initial_mid, final_mid) VALUES (?, ?, ?, ?);", (season_n, sid, initial_mid, final_mid,))
    
    conn.commit()
    conn.close()
    
    return c.lastrowid

def get_season(sid=-1):
    
    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()
    
    # If sid == -1, use the last season
    
    if sid == -1:
        c.execute("SELECT sid FROM Seasons ORDER BY sid DESC;")
        
        sid = c.fetchone()[0]
    
    c.execute("SELECT * FROM Seasons WHERE sid=?;", (sid,))
    
    season_db = c.fetchone()
    
    if season_db == None: return None
    
    season = Season(*season_db)
    
    conn.close()
      
    return season

def get_season_n(sid):
    
    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()
    
    c.execute("SELECT season_n FROM Seasons WHERE sid=?;", (sid,))
    
    season_n = c.fetchone()[0]
    
    conn.close()
      
    return season_n

def get_last_sid():
        
    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()
    
    c.execute("SELECT sid FROM Seasons ORDER BY sid DESC;")
    
    sid = c.fetchone()[0]
    
    conn.close()
      
    return sid

def get_last_season_n():
    
    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()
    
    c.execute("SELECT season_n FROM Seasons ORDER BY season_n DESC;")
    
    season_n = c.fetchone()[0]
    
    conn.close()
      
    return season_n


def set_goal(mid, pid, goal_time, figure, legal):

    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()        
    
    c.execute("INSERT INTO Goals (mid, pid, goal_time, figure, legal) VALUES (?, ?, ?, ?, ?);", (mid, pid, goal_time, figure, legal,))
 
    conn.commit()
    conn.close()
    
    return c.lastrowid

def get_goals(mid=-1, pid=-1):
    
    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()
    
    if mid != -1:
        c.execute("SELECT * FROM Goals WHERE mid=?;", (mid,))
    elif pid != -1:
        c.execute("SELECT * FROM Goals WHERE pid=?;", (pid,))
    
    goals_db = c.fetchall()
        
    goals = [Goal(*g) for g in goals_db]
    
    conn.close()
      
    return goals


def set_championship (teams):
    
    # Variables teams is in the format [[1,2], [3,4], [5,6] ... [15,16]]
    
    match_str = []
    
    for i in range(0, len(teams), 2):
        match_str.append("%d, %d, %d, %d, -1, -1" % (teams[i][0], teams[i][1], teams[i+1][0], teams[i+1][1]))
    
    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()        
    
    c.execute("INSERT INTO Championships (match_1, match_2, match_3, match_4) VALUES (?, ?, ?, ?);", match_str)
 
    conn.commit()
    conn.close()
    
    return c.lastrowid

def get_championship(cid=-1):
    
    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()
    
    if cid == -1:
        
        # Get the last cid
        
        c.execute("SELECT cid FROM Championships ORDER BY cid DESC;")
        
        cid = c.fetchone()
        
        if cid == None:
            return Championship(-1, '-1', '-1', '-1', '-1', '-1', '-1', '-1', '-1', '-1', 1)
        
        cid = cid[0]
            
    c.execute("SELECT * FROM Championships WHERE cid=?;", (cid,))
    
    championship_db = c.fetchone()
    
    if championship_db == None: return None
        
    championship = Championship(*championship_db)
        
    conn.close()
      
    return championship

def update_championship(champ):
    
    # cid, match_1, match_2, match_3, match_4, match_5, match_6, match_7, winner, runner_up, finished
    
    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()        
    
    for i in range(len(champ.matches)):
        
        c.execute("UPDATE Championships SET match_%d = ? WHERE cid = ?;" % (i+1), ((', '.join([str(e) for e in champ.matches[i]])), champ.cid))
 
    c.execute("UPDATE Championships SET winner = ?    WHERE cid = ?;", ((', '.join([str(e) for e in champ.winner])),    champ.cid))
    c.execute("UPDATE Championships SET runner_up = ? WHERE cid = ?;", ((', '.join([str(e) for e in champ.runner_up])), champ.cid))
    c.execute("UPDATE Championships SET finished = ?  WHERE cid = ?;", (champ.finished,  champ.cid))
 
    conn.commit()
    conn.close()
    
    return


" *** Match Stats Functions *** "

def get_season_legal_goals(pid):
    
    # Get current season initial mid
    
    initial_mid = get_season().final_mid + 1
    
    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()
    
    # Get legal goals
    
    c.execute("SELECT COUNT(*) FROM Goals WHERE (mid >= ? AND pid = ? AND legal = 1);", (initial_mid, pid,))
    
    result = c.fetchone()
    
    if result != None:
        n_legal_goals = result[0]
    else:
        n_legal_goals = 0
        
    # Get total goals
        
    c.execute("SELECT COUNT(*) FROM Goals WHERE (mid >= ? AND pid = ?);", (initial_mid, pid,))
    
    result = c.fetchone()
    
    if result != None:
        n_total_goals = result[0]
    else:
        n_total_goals = 0
            
    conn.close()
      
    return (n_legal_goals, n_total_goals)

def get_season_illegal_goals(pid):
    
    # Get current season initial mid
    
    initial_mid = get_season().final_mid + 1
    
    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()
    
    c.execute("SELECT COUNT(*) FROM Goals WHERE (mid >= ? AND pid = ? AND legal = 0);", (initial_mid, pid,))
    
    result = c.fetchone()
    
    if result != None:
        n_illegal_goals = result[0]
    else:
        n_illegal_goals = 0
        
    # Get total goals
        
    c.execute("SELECT COUNT(*) FROM Goals WHERE (mid >= ? AND pid = ?);", (initial_mid, pid,))
    
    result = c.fetchone()
    
    if result != None:
        n_total_goals = result[0]
    else:
        n_total_goals = 0
            
    conn.close()
      
    return (n_illegal_goals, n_total_goals)
    
def get_season_goals_against(pid):
    
    # Get current season initial mid
    
    initial_mid = get_season().final_mid + 1
    
    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()
    
    c.execute("SELECT SUM(score_mad) FROM Matches WHERE (mid >= ? AND atl_def_pid = ?);", (initial_mid, pid,))
    
    result = c.fetchone()[0]
        
    n_goals = 0
    
    if result != None:
        n_goals += result
    
    c.execute("SELECT SUM(score_atl) FROM Matches WHERE (mid >= ? AND mad_def_pid = ?);", (initial_mid, pid,))
    
    result = c.fetchone()[0]
    
    if result != None:
        n_goals += result
            
    conn.close()
      
    return (n_goals,)

def get_season_matches_won(pid):
    
    # Get current season initial mid
    
    initial_mid = get_season().final_mid + 1
    
    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()
    
    # Get matches won
    
    c.execute("SELECT COUNT(*) FROM Matches WHERE (mid >= ? AND (mad_def_pid = ? OR mad_atk_pid = ?) AND score_mad > score_atl);", (initial_mid, pid, pid))
    
    result = c.fetchone()
    
    n_wins = 0

    if result != None:
        n_wins += result[0]
    
    c.execute("SELECT COUNT(*) FROM Matches WHERE (mid >= ? AND (atl_def_pid = ? OR atl_atk_pid = ?) AND score_atl > score_mad);", (initial_mid, pid, pid))
    
    result = c.fetchone()
    
    if result != None:
        n_wins += result[0]
        
    # Get total matches played
    
    c.execute("SELECT COUNT(*) FROM Matches WHERE (mid >= ? AND (mad_def_pid = ? OR mad_atk_pid = ? OR atl_def_pid = ? OR atl_atk_pid = ?));", (initial_mid, pid, pid, pid, pid))
    
    result = c.fetchone()
    
    if result != None:
        n_matches = result[0]
    else:
        n_matches = 0
            
    conn.close()
      
    return (n_wins, n_matches)

def get_season_matches_lost(pid):
    
    # Get current season initial mid
    
    initial_mid = get_season().final_mid + 1
    
    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()
    
    # Get matches lost
    
    c.execute("SELECT COUNT(*) FROM Matches WHERE (mid >= ? AND (mad_def_pid = ? OR mad_atk_pid = ?) AND score_mad < score_atl);", (initial_mid, pid, pid))
    
    result = c.fetchone()
    
    n_losses = 0

    if result != None:
        n_losses += result[0]
    
    c.execute("SELECT COUNT(*) FROM Matches WHERE (mid >= ? AND (atl_def_pid = ? OR atl_atk_pid = ?) AND score_atl < score_mad);", (initial_mid, pid, pid))
    
    result = c.fetchone()
    
    if result != None:
        n_losses += result[0]
        
    # Get total matches played
    
    c.execute("SELECT COUNT(*) FROM Matches WHERE (mid >= ? AND (mad_def_pid = ? OR mad_atk_pid = ? OR atl_def_pid = ? OR atl_atk_pid = ?));", (initial_mid, pid, pid, pid, pid))
    
    result = c.fetchone()
    
    if result != None:
        n_matches = result[0]
    else:
        n_matches = 0
            
    conn.close()
      
    return (n_losses, n_matches)

def get_team_matches_won(pid_1, pid_2):
    
    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()
    
    # Get matches lost
    
    c.execute("SELECT COUNT(*) FROM Matches WHERE ((mad_def_pid = ? AND mad_atk_pid = ?) OR \
                                                   (mad_def_pid = ? AND mad_atk_pid = ?)) AND score_mad > score_atl;", (pid_1, pid_2, pid_2, pid_1))
    
    result = c.fetchone()
    
    n_wins = 0

    if result != None:
        n_wins += result[0]
    
    c.execute("SELECT COUNT(*) FROM Matches WHERE ((atl_def_pid = ? AND atl_atk_pid = ?) OR \
                                                   (atl_def_pid = ? AND atl_atk_pid = ?)) AND score_atl > score_mad;", (pid_1, pid_2, pid_2, pid_1))
    
    result = c.fetchone()
    
    if result != None:
        n_wins += result[0]
        
    # Get total matches played
    
    c.execute("SELECT COUNT(*) FROM Matches WHERE (mad_def_pid = ? AND mad_atk_pid = ? OR \
                                                   mad_def_pid = ? AND mad_atk_pid = ? OR \
                                                   atl_def_pid = ? AND atl_atk_pid = ? OR \
                                                   atl_def_pid = ? AND atl_atk_pid = ?) AND score_mad != score_atl;", (pid_1, pid_2, pid_2, pid_1, pid_1, pid_2, pid_2, pid_1))
    
    result = c.fetchone()
    
    if result != None:
        n_matches = result[0]
    else:
        n_matches = 0
            
    conn.close()
      
    return (n_wins, n_matches)

def get_team_matches_lost(pid_1, pid_2):
    
    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()
    
    # Get matches lost
    
    c.execute("SELECT COUNT(*) FROM Matches WHERE ((mad_def_pid = ? AND mad_atk_pid = ?) OR \
                                                   (mad_def_pid = ? AND mad_atk_pid = ?)) AND score_mad < score_atl;", (pid_1, pid_2, pid_2, pid_1))
    
    result = c.fetchone()
    
    n_losses = 0

    if result != None:
        n_losses += result[0]
    
    c.execute("SELECT COUNT(*) FROM Matches WHERE ((atl_def_pid = ? AND atl_atk_pid = ?) OR \
                                                   (atl_def_pid = ? AND atl_atk_pid = ?)) AND score_atl < score_mad;", (pid_1, pid_2, pid_2, pid_1))
    
    result = c.fetchone()
    
    if result != None:
        n_losses += result[0]
        
    # Get total matches played
    
    c.execute("SELECT COUNT(*) FROM Matches WHERE (mad_def_pid = ? AND mad_atk_pid = ? OR \
                                                   mad_def_pid = ? AND mad_atk_pid = ? OR \
                                                   atl_def_pid = ? AND atl_atk_pid = ? OR \
                                                   atl_def_pid = ? AND atl_atk_pid = ?) AND score_mad != score_atl;", (pid_1, pid_2, pid_2, pid_1, pid_1, pid_2, pid_2, pid_1))
    
    result = c.fetchone()
    
    if result != None:
        n_matches = result[0]
    else:
        n_matches = 0
            
    conn.close()
      
    return (n_losses, n_matches)
    


" *** Populate DB functions *** "

def populate_players_table():
        
    players_file = open("db/Players.csv", 'r')
    
    players_data = [codecs.decode(i, 'latin-1')[:-1].split(',') for i in players_file]
    players_data = players_data[1:]
    
    players_file.close()
    
    for i in range(len(players_data)):
        for j in range(len(players_data[i])):
            if players_data[i][j] == '':
                players_data[i][j] = "NO DATA"    
    
    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    
    c = conn.cursor()                                    
        
    for player_data in players_data:
        c.execute(u"INSERT INTO Players VALUES (%s, '%s', '%s', '%s', %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);" % tuple(player_data))
    
    conn.commit()
    conn.close()

    return

def update_players_table_data(players_list=[]):
    
    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)

    c = conn.cursor()       
    
    # If not provided, use all pids
    
    if players_list == []:
        players_list = range(71)
    
    for pid in players_list:
        
        player_data = []
        
        # Get n_matches
        
        c.execute("SELECT COUNT(*) FROM Matches WHERE (mad_def_pid=? or mad_atk_pid=? or atl_def_pid=? or atl_atk_pid=?);", (pid, pid, pid, pid))
        player_data.append(c.fetchone()[0])
        
        # Get play time
        
        c.execute("SELECT SUM(match_time) FROM Matches WHERE (mad_def_pid=? or mad_atk_pid=? or atl_def_pid=? or atl_atk_pid=?);", (pid, pid, pid, pid))
        
        play_time = c.fetchone()[0]
        
        if play_time == None:
            player_data.append(0)
        else:
            player_data.append(play_time)
        
        # Get mad_wins
        
        c.execute("SELECT COUNT(*) FROM Matches WHERE ((mad_def_pid=? or mad_atk_pid=?) AND score_mad > score_atl);", (pid, pid))
        player_data.append(c.fetchone()[0])
        
        # Get mad_losses
        
        c.execute("SELECT COUNT(*) FROM Matches WHERE ((mad_def_pid=? or mad_atk_pid=?) AND score_mad < score_atl);", (pid, pid))
        player_data.append(c.fetchone()[0])
        
        # Get atl_wins
        
        c.execute("SELECT COUNT(*) FROM Matches WHERE ((atl_def_pid=? or atl_atk_pid=?) AND score_mad < score_atl);", (pid, pid))
        player_data.append(c.fetchone()[0])
        
        # Get atl_losses
        
        c.execute("SELECT COUNT(*) FROM Matches WHERE ((atl_def_pid=? or atl_atk_pid=?) AND score_mad > score_atl);", (pid, pid))
        player_data.append(c.fetchone()[0])
        
        # Get def_wins
        
        c.execute("SELECT COUNT(*) FROM Matches WHERE ((mad_def_pid=? AND score_mad > score_atl) OR (atl_def_pid=? AND score_mad < score_atl));", (pid, pid))
        player_data.append(c.fetchone()[0])
        
        # Get def_losses
        
        c.execute("SELECT COUNT(*) FROM Matches WHERE ((mad_def_pid=? AND score_mad < score_atl) OR (atl_def_pid=? AND score_mad > score_atl));", (pid, pid))
        player_data.append(c.fetchone()[0])
        
        # Get atk_wins
        
        c.execute("SELECT COUNT(*) FROM Matches WHERE ((mad_atk_pid=? AND score_mad > score_atl) OR (atl_atk_pid=? AND score_mad < score_atl));", (pid, pid))
        player_data.append(c.fetchone()[0])
        
        # Get atk_losses
        
        c.execute("SELECT COUNT(*) FROM Matches WHERE ((mad_atk_pid=? AND score_mad < score_atl) OR (atl_atk_pid=? AND score_mad > score_atl));", (pid, pid))
        player_data.append(c.fetchone()[0])
        
        # Get n_goals
        
        c.execute("SELECT COUNT(*) FROM Goals WHERE (pid=? AND legal=1);", (pid,))
        player_data.append(c.fetchone()[0])
        
        # Get n_autogoals
        
        c.execute("SELECT COUNT(*) FROM Goals WHERE (pid=? AND legal=0);", (pid,))
        player_data.append(c.fetchone()[0])
        
        # Get n_rcvd_goals
        
        c.execute("SELECT SUM(score_atl) FROM Matches WHERE (mad_def_pid=?);", (pid,))
        
        n_rcvd_goals = c.fetchone()[0]
        
        if n_rcvd_goals == None:
            n_rcvd_goals = 0
        
        c.execute("SELECT SUM(score_mad) FROM Matches WHERE (atl_def_pid=?);", (pid,))
        
        n_rcvd_goals_temp = c.fetchone()[0]
        
        if n_rcvd_goals_temp != None:
            n_rcvd_goals += n_rcvd_goals_temp
        
        player_data.append(n_rcvd_goals)
        
        # Get n_rcvd_goals_helper
        
        c.execute("SELECT SUM(score_atl) FROM Matches WHERE (mad_atk_pid=?);", (pid,))
        
        n_rcvd_goals_helper = c.fetchone()[0]
        
        if n_rcvd_goals_helper == None:
            n_rcvd_goals_helper = 0
        
        c.execute("SELECT SUM(score_mad) FROM Matches WHERE (atl_atk_pid=?);", (pid,))
        
        n_rcvd_goals_helper_temp = c.fetchone()[0]
        
        if n_rcvd_goals_helper_temp != None:
            n_rcvd_goals_helper += n_rcvd_goals_helper_temp
        
        player_data.append(n_rcvd_goals_helper)
        
        # Append pid at the end
        
        player_data.append(pid)
        
        # Update Players Table
                        
        c.execute("UPDATE Players SET n_matches=?, play_time=?, mad_wins=?, mad_losses=?, atl_wins=?, atl_losses=?,\
                   def_wins=?, def_losses=?, atk_wins=?, atk_losses=?, n_goals=?, n_autogoals=?, n_rcvd_goals=?,\
                   n_rcvd_goals_helper=? WHERE pid=?;", tuple(player_data))
        
    conn.commit()
    conn.close()
    
    return

def populate_matches_table():
        
    matches_file = open("db/matches.csv", 'r')
    
    matches_data = [i[:-1].split(',') for i in matches_file]
    matches_data = matches_data[1:]
    
    matches_file.close()
    
    # If date is in format DD-MM-YYYY, so we need to change it to YYYY-MM-DD
    
    #for i in range(len(matches_data)):
    #    old_date = matches_data[i][7]
    #    new_date = old_date[6:] + '-' + old_date[3:5] + '-' + old_date[0:2]
    #    matches_data[i][7] = new_date
        
    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()
        
    for match_data in matches_data:
        c.execute(u"INSERT INTO Matches VALUES (%s, %s, %s, %s, %s, %s, %s, '%s', '%s', %s);" % tuple(match_data))
    
    conn.commit()
    conn.close()

    return

def populate_seasons_table():
        
    seasons_file = open("db/Seasons.csv", 'r')
    
    seasons_data = [i[:-1].split(',') for i in seasons_file]
    seasons_data = seasons_data[1:]
    
    seasons_file.close()
    
    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()                                    
        
    for season_data in seasons_data:
        c.execute(u"INSERT INTO Seasons VALUES (%s, %s, %s, %s);" % tuple(season_data))
    
    conn.commit()
    conn.close()

    return

def populate_goals_table():
        
    goals_file = open("db/goals.csv", 'r')
    
    goals_data = [i[:-1].split(',') for i in goals_file]
    goals_data = goals_data[1:]
    
    goals_file.close()
    
    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    
    c = conn.cursor()                                    
        
    for goal_data in goals_data:
        c.execute(u"INSERT INTO Goals VALUES (%s, %s, %s, %s, %s);" % tuple(goal_data))
    
    conn.commit()
    conn.close()

    return


" *** Global *** "

if 'dev' in sys.argv:            
    db_name = "db/futbolin_dev.db"          # For development

else:
    if platform.system() == "Windows":
        db_name = "db/futbolin_windows.db"  # For deployment testing in Windows. This way, the DB file won't be overwritten by mistake when transfering file to RPi
    else:
        db_name = "db/futbolin.db"          # For deployment


if __name__ == "__main__":
    pass
   








