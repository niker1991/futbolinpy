'''
Created on 20/4/2015

@author: Nicanor Romero Venier
'''

from gi.repository import Gtk
from gi.repository import GObject
from gi.repository import GdkPixbuf
from gi.repository import Gdk
from gi.repository import GLib

from time import sleep
from logging import handlers
import time
import db_handler
import twitter_handler
import ConfigParser
import datetime
import threading
import sys
import shutil
import logging
import random
import subprocess
import urllib
import bottle


class FutbolinGUI():

    " *** Initialization Functions *** "
 
    def set_widget_names(self):
        """
        Description:
            This function sets an identifier to some objects of the GUI so that
            they are traceable afterwards. This comes handy when many GUI elements
            callback to the same function. There they can be identified by their
            parameter 'name' which is obtained with the method get_name. 

        Arguments:
            None.
    
        Returns:
            None.
    
        """
        
        # Set identifiers for characters
    
        for i in range(1, 17):
                
            self.builder.get_object("ebox_ch_%d" % i).set_name("%d" % i)
            
        # Set identifiers for player buttons (In characters screen)
        
        for i in range(1, 5):
            
            self.builder.get_object("ebox_p%d" % i).set_name("%d" % i)
            
        # Set identifiers for scorers buttons
        
        for i in range(1, 12):
            
            self.builder.get_object("ebox_mad_%d" % i).set_name("%d" % i)
            self.builder.get_object("ebox_atl_%d" % i).set_name("%d" % (i + 11))
            
        # Set identifiers for teams (In new championship screen)
    
        for i in range(1, 9):
                
            self.builder.get_object("ebox_b_team_%d" % i).set_name("%d" % i)
            
        # Set identifiers for team characters (In select team screen of new championship)
    
        for i in range(1, 9):
                
            self.builder.get_object("ebox_team_ch_%d" % i).set_name("%d" % i)
            
        # Set identifiers for championship matches 
    
        for i in range(1, 8):
            self.builder.get_object("ebox_b_m_team_%d" % (i*2-1)).set_name("%d" % i)
            self.builder.get_object("ebox_b_m_team_%d" % (i*2)).set_name("%d" % i)
        
        return
    

    " *** Show Screen Functions *** "

    def show_characters_screen(self):
        """
        Description:
            This function shows the widgets from the characters screen, updates
            the rows of characters and hides the widgets from all the other
            possible screens.
            This function also launches a thread to animate the Start Match button.

        Arguments:
            None.
    
        Returns:
            None.
    
        """
        
        self.current_screen = "characters"
        
        # Hide any widgets from the possible previous screens
        
        for widget in self.widgets_main_menu:
            self.builder.get_object(widget).hide()
            
        for widget in self.widgets_match:
            self.builder.get_object(widget).hide()
            
        for widget in self.widgets_ask_confirmation:
            self.builder.get_object(widget).hide()
            
        for widget in self.widgets_congratulations:
            self.builder.get_object(widget).hide()
            
        # Show main widgets 
        
        for widget in self.widgets_main:
            self.builder.get_object(widget).show()
            
        # Show characters widgets
            
        for widget in self.widgets_characters:
            self.builder.get_object(widget).show()
            
        # Hide up button (current row always set to 1)

        self.builder.get_object("ebox_b_up").hide()
            
        # Update characters rows (Always reset current row to 1)
                
        self.current_row = 1
                
        self.update_rows_characters()
        
        # Restore normal background (in case of coming back from mundialito)
        
        self.builder.get_object("background").set_from_file(self.background_image)
        
        # Hide mundialito button images
        
        for i in range(1, 5):
            self.builder.get_object("mundialito_%d" % i).hide()
            
        # Start new thread to animate Start Match button
        
        self.start_thread(self.animate_start_button)
        
        return
    
    def show_ask_confirmation_screen(self):
        """
        Description:
            This function shows the widgets from the ask confirmation screen,
            sets the image for the quit question object and hides the widgets
            from all the other possible screens.

        Arguments:
            None.
    
        Returns:
            None.
    
        """
           
        # Change image depending on context ?
        
        if self.current_screen == "match" and self.match_b_clicked == "reset":
            image_file = "ask_confirmation_reset.png"
            
        elif self.current_screen == "championships":
            image_file = "ask_confirmation_new_championship.png"
        
        else:
            image_file = "ask_confirmation_exit.png"
            
        self.builder.get_object("quit_question").set_from_file("images/%s" % image_file)
            
        # Hide any widgets from the possible previous screens
        
        for widget in self.widgets_main:
            self.builder.get_object(widget).hide()
            
        for widget in self.widgets_characters:
            self.builder.get_object(widget).hide()
            
        for widget in self.widgets_match:
            self.builder.get_object(widget).hide()
            
        for widget in self.widgets_championships:
            self.builder.get_object(widget).hide()
            
        # Show ask quit question widgets
            
        for widget in self.widgets_ask_confirmation:
            self.builder.get_object(widget).show()
            
        return
    
    def show_match_screen(self, use_threads=True):
        """
        Description:
            This function shows the widgets from the match screen,
            and hides the widgets from all the other possible screens.

        Arguments:
            use_threads (boolean): This argument defines whether to use or not
            threads inside the function. Threads are not used
            when the call comes from the web service (remote control).
    
        Returns:
            None.
    
        """
        
        self.current_screen = "match"
        
        # Hide any widgets from the possible previous screens
        
        for widget in self.widgets_characters:
            self.builder.get_object(widget).hide()
            
        for widget in self.widgets_ask_scorer:
            self.builder.get_object(widget).hide()
            
        for widget in self.widgets_ask_legal:
            self.builder.get_object(widget).hide()
            
        for widget in self.widgets_championship_matches:
            self.builder.get_object(widget).hide()
            
        for widget in self.widgets_championship_match:
            self.builder.get_object(widget).hide()
            
        for widget in self.widgets_ask_confirmation:
            self.builder.get_object(widget).hide()
            
        # Show main widgets
        
        for widget in self.widgets_main:
            self.builder.get_object(widget).show()
            
        # Show match widgets
        
        for widget in self.widgets_match:
            
            # Skip End Match button during match
            
            if widget == "ebox_b_end_match" and self.in_match:
                self.builder.get_object(widget).hide()
                continue
            
            # Skip Mundialito message if not in mundialito mode
            
            if widget == "mundialito_msg" and not self.mundialito_mode:
                self.builder.get_object(widget).hide()
                continue
            
            self.builder.get_object(widget).show()
         
        # Update match screen in thread to speed up response
         
        if use_threads:
            self.start_thread(self.update_match_screen, (use_threads,))
        else:
            self.update_match_screen(use_threads)        
            
        return
    
    def show_ask_scorer_screen(self):
        """
        Description:
            This function shows the widgets from the match screen,
            sets the initial value for the timeout string and hides
            the widgets from all the other possible screens.

        Arguments:
            None.
            
        Returns:
            None.
    
        """
        
        self.current_screen = "ask_scorer"
        
        # Set text in Ask Legal timeout
        
        self.builder.get_object("legal_timeout").set_text(str(self.ask_timeout))
        
        # Hide any widgets from the possible previous screens
        
        for widget in self.widgets_main:
            self.builder.get_object(widget).hide()
        
        for widget in self.widgets_match:
            self.builder.get_object(widget).hide()
        
        # Show Ask Scorer widgets
        
        for widget in self.widgets_ask_scorer:
            self.builder.get_object(widget).show()
            
        return
    
    def show_ask_legal_screen(self, use_threads=True):
        """
        Description:
            This function shows the widgets from the ask legal screen,
            sets the initial value for the timeout string and hides
            the widgets from all the other possible screens.

        Arguments:
            use_threads (boolean): This argument defines whether to use or not
            threads inside the function. Threads are not used
            when the call comes from the web service (remote control).
            
        Returns:
            None.
    
        """
        
        self.current_screen = "ask_legal"
        
        if not use_threads:
            Gdk.threads_enter()
            
        # Set text in Ask Legal timeout
        
        self.builder.get_object("legal_timeout").set_text(str(self.ask_timeout))
        
        # Hide any widgets from the possible previous screens
        
        for widget in self.widgets_main:
            self.builder.get_object(widget).hide()
        
        for widget in self.widgets_match:
            self.builder.get_object(widget).hide()
            
        for widget in self.widgets_ask_scorer:
            self.builder.get_object(widget).hide()
            
        # Show Ask Legal widgets
        
        for widget in self.widgets_ask_legal:
            self.builder.get_object(widget).show()
            
        if not use_threads:
            Gdk.threads_leave()
            
        return
    
    def show_main_menu_screen(self):
        """
        Description:
            This function shows the widgets from the main menu screen,
            sets the image for the main menu header and hides
            the widgets from all the other possible screens.

        Arguments:
            None.
            
        Returns:
            None.
    
        """
        
        self.current_screen = "main_menu"
        
        # Hide any widgets from the possible previous screens
        
        for widget in self.widgets_main:
            self.builder.get_object(widget).hide()
            
        for widget in self.widgets_characters:
            self.builder.get_object(widget).hide()
            
        for widget in self.widgets_championships:
            self.builder.get_object(widget).hide()
            
        for widget in self.widgets_seasons:
            self.builder.get_object(widget).hide()
            
        for widget in self.widgets_stats:
            self.builder.get_object(widget).hide()
        
        # Show Main Menu widgets
        
        self.builder.get_object("main_menu_header").set_from_file("images/main_menu_header.png")
        
        for widget in self.widgets_main_menu:
            self.builder.get_object(widget).show()
        
        return
    
    def show_championships_screen(self):
        """
        Description:
            This function shows the widgets from the championship screen,
            sets the image for the main menu header and hides
            the widgets from all the other possible screens.

        Arguments:
            None.
            
        Returns:
            None.
    
        """
        
        self.current_screen = "championships"
        
        # Hide any widgets from the possible previous screens
        
        for widget in self.widgets_main_menu:
            self.builder.get_object(widget).hide()
            
        for widget in self.widgets_new_championship:
            self.builder.get_object(widget).hide()    
            
        for widget in self.widgets_championship_matches:
            self.builder.get_object(widget).hide()
            
        for widget in self.widgets_ask_confirmation:
            self.builder.get_object(widget).hide()           
        
        # Show Championships widgets
        
        self.builder.get_object("main_menu_header").set_from_file("images/championships_header.png")
        
        for widget in self.widgets_championships:
            self.builder.get_object(widget).show()
            
        # Show or Hide Continue button 
        
        if self.current_championship.finished:
            self.builder.get_object("ebox_b_continue_championship").hide()
        
        return
    
    def show_new_championship_screen(self):
        """
        Description:
            This function shows the widgets from the new championship
            screen, sets the image for the main menu header and hides
            the widgets from all the other possible screens.

        Arguments:
            None.
            
        Returns:
            None.
    
        """
        
        self.current_screen = "new_championship"
        
        # Hide any widgets from the possible previous screens
        
        for widget in self.widgets_championships:
            self.builder.get_object(widget).hide()
            
        for widget in self.widgets_select_team:
            self.builder.get_object(widget).hide()       
            
        for widget in self.widgets_ask_confirmation:
            self.builder.get_object(widget).hide()                 
        
        # Show New Championship widgets
        
        self.builder.get_object("main_menu_header").set_from_file("images/new_championship_header.png")
        
        for widget in self.widgets_new_championship:
            self.builder.get_object(widget).show()
            
        # Show or Hide the Create Championship button
        
        if [] in self.championship_teams:
            self.builder.get_object("ebox_b_create_championship").hide()
        
        return
    
    def show_select_team_screen(self):
        """
        Description:
            This function shows the widgets from the select team screen,
            which is within the championship mode. It manages the images
            of the selected players and also updates the row of characters.
            It also sets the image for the main menu header and hides
            the widgets from all the other possible screens.

        Arguments:
            None.
            
        Returns:
            None.
    
        """
        
        self.current_screen = "select_team"
        
        # Hide any widgets from the possible previous screens
        
        for widget in self.widgets_new_championship:
            self.builder.get_object(widget).hide()               
        
        # Reset the players images if empty
        
        if self.championship_teams[self.last_team_selected - 1] == []:
            self.builder.get_object("team_p1").set_from_file("images/team_player_1.png")
            self.builder.get_object("team_p2").set_from_file("images/team_player_2.png")
            
        else:            
            self.team_selected_players[0] = [self.championship_teams[self.last_team_selected - 1][0]]
            self.team_selected_players[1] = [self.championship_teams[self.last_team_selected - 1][1]]
            
            self.builder.get_object("team_p1").set_from_file("images/characters/ch_%d.png" % self.championship_teams[self.last_team_selected - 1][0])
            self.builder.get_object("team_p2").set_from_file("images/characters/ch_%d.png" % self.championship_teams[self.last_team_selected - 1][1])
        
        # Show Select Team widgets
        
        self.builder.get_object("main_menu_header").set_from_file("images/select_team_header.png")
        
        for widget in self.widgets_select_team:
            self.builder.get_object(widget).show()
            
        # Hide up button (current row always set to 1)

        self.builder.get_object("ebox_b_up_2").hide()   
        
        # Update row of characters (Always reset to row 1)
        
        self.current_row = 1
            
        self.update_team_rows_characters()
        
        # Reset variable
        
        self.last_team_ch_clicked = -1
        
        return
    
    def show_championship_matches_screen(self):
        """
        Description:
            This function shows the widgets from the championship
            matches screen, updates the images for each match,
            sets the image for the main menu header and hides
            the widgets from all the other possible screens.

        Arguments:
            None.
            
        Returns:
            None.
    
        """
        
        self.current_screen = "championship_matches"
        
        # Hide any widgets from the possible previous screens
        
        for widget in self.widgets_new_championship:
            self.builder.get_object(widget).hide()
            
        for widget in self.widgets_championships:
            self.builder.get_object(widget).hide()
            
        for widget in self.widgets_match:
            self.builder.get_object(widget).hide()
            
        for widget in self.widgets_main:
            self.builder.get_object(widget).hide()
            
        for widget in self.widgets_championship_match:
            self.builder.get_object(widget).hide()
            
        for widget in self.widgets_ask_confirmation:
            self.builder.get_object(widget).hide()
        
        # Show Championship Matches widgets
        
        self.builder.get_object("main_menu_header").set_from_file("images/championship_matches_header.png")
        
        for widget in self.widgets_championship_matches:
            self.builder.get_object(widget).show()
            
        # Set the files for each match in the championship
        
        self.update_championship_matches()         
        
        return
    
    def show_championship_match_screen(self):
        """
        Description:
            This function shows the widgets from the championship
            match screen, launches a thread to animate the start match
            button and hides the widgets from all the other possible screens.

        Arguments:
            None.
            
        Returns:
            None.
    
        """
        
        self.current_screen = "championship_match"
        
        # Hide any widgets from the possible previous screens
        
        for widget in self.widgets_championship_matches:
            self.builder.get_object(widget).hide()
            
        for widget in self.widgets_championships:
            self.builder.get_object(widget).hide()
            
        for widget in self.widgets_match:
            self.builder.get_object(widget).hide()
        
        # Show Championship Match widgets
                
        for widget in self.widgets_championship_match:
            self.builder.get_object(widget).show()
            
        for widget in self.widgets_main:
            self.builder.get_object(widget).show()
            
        # Start new thread to animate Start Match button
        
        self.start_thread(self.animate_start_button)
            
        return
    
    def show_congratulations_screen(self):
        """
        Description:
            This function shows the widgets from the congratulations screen,
            sets the image for the main menu header and hides
            the widgets from all the other possible screens.

        Arguments:
            None.
            
        Returns:
            None.
    
        """
                
        self.current_screen = "congratulations"
        
        # Hide any widgets from the possible previous screens
        
        for widget in self.widgets_match:
            self.builder.get_object(widget).hide()
            
        for widget in self.widgets_main:
            self.builder.get_object(widget).hide()
        
        # Show Congratulations widgets
        
        self.builder.get_object("main_menu_header").set_from_file("images/congratulations_header.png")
        
        for widget in self.widgets_congratulations:
            self.builder.get_object(widget).show()     
        
        return
    
    def show_seasons_screen(self):
        """
        Description:
            This function shows the widgets from the seasons screen,
            and hides the widgets from all the other possible screens.

        Arguments:
            None.
            
        Returns:
            None.
    
        """
        
        self.current_screen = "seasons"
        
        # Hide any widgets from the possible previous screens
        
        for widget in self.widgets_main_menu:
            self.builder.get_object(widget).hide()                    
        
        # Show Seasons widgets
        
        for widget in self.widgets_seasons:
            self.builder.get_object(widget).show()
        
        return
    
    def show_stats_screen(self):
        """
        Description:
            This function shows the widgets from the stats screen,
            and hides the widgets from all the other possible screens.

        Arguments:
            None.
            
        Returns:
            None.
    
        """
        
        self.current_screen = "stats"
        
        # Hide any widgets from the possible previous screens
        
        for widget in self.widgets_main_menu:
            self.builder.get_object(widget).hide()                    
        
        # Show Stats widgets
        
        self.builder.get_object("main_menu_header").set_from_file("images/stats_header.png")
        
        ch = self.builder.get_object("stats_ch")  
            
        big_ch = GdkPixbuf.Pixbuf.new_from_file_at_size('images/characters/ch_%d.png' % self.ch_order[self.stats_ch_i], 210, 240)
        
        ch.set_from_pixbuf(big_ch)
                    
        for widget in self.widgets_stats:
            self.builder.get_object(widget).show()
            
        # Hide left button 
            
        self.builder.get_object("ebox_b_stats_left").hide() 
        
        return
    
    
    " *** Update Screen Functions *** "

    def update_match_screen(self, use_threads=True):
        """
        Description:
            This function updates all the dynamic widgets from the
            match screen. It updates the score board, the remaining balls,
            the goal dash and the match stats.

        Arguments:
            use_threads (boolean): This argument defines whether to use or not
            threads inside the function. Threads are not used
            when the call comes from the web service (remote control).
            
        Returns:
            None.
    
        """
        
        if use_threads:
            Gdk.threads_enter()
        
        # Update scoreboard contents
        
        self.update_scoreboard()
        
        # Update remaining balls
        
        self.update_remaining_balls()
        
        # Update goal dash
        
        self.update_goal_dash()
        
        # Hide match stats if there aren't any
        
        self.show_match_stats()
        
        if use_threads:
            Gdk.threads_leave()
        
        return

    def update_rows_characters(self):
        """
        Description:
            This function updates the characters shown in the two rows
            displayed in the characters screen and in the championship
            select team screen. Depending on the value of current_row, it
            chooses the files to show in each row.
            It also shows or hides the buttons to go up or down the rows.

        Arguments:
            None.
            
        Returns:
            None.
    
        """
        
        self.ch_first_row = self.ch_order[(self.current_row - 1) * 8: self.current_row * 8]
        self.ch_second_row = self.ch_order[self.current_row * 8: (self.current_row + 1) * 8]    
        
        for i in range(len(self.ch_first_row)):
            ch = self.builder.get_object("ch_%d" % (i+1))   
            ch.set_from_file("images/characters/ch_%d.png" % self.ch_first_row[i])
            self.builder.get_object("ebox_ch_%d" % (i+1)).show()              
        
        for i in range(len(self.ch_first_row), 8):
            self.builder.get_object("ebox_ch_%d" % (i+1)).hide() 
        
        for i in range(len(self.ch_second_row)):
            ch = self.builder.get_object("ch_%d" % (i+9))  
            
            small_ch = GdkPixbuf.Pixbuf.new_from_file_at_size('images/characters/ch_%d.png' % self.ch_second_row[i], 87, 100)
            ch.set_from_pixbuf(small_ch)
                                          
            self.builder.get_object("ebox_ch_%d" % (i+9)).show()                
    
        for i in range(len(self.ch_second_row), 8):
            self.builder.get_object("ebox_ch_%d" % (i+9)).hide() 
                
        return 
    
    def update_scoreboard(self):
        """
        Description:
            This function updates the score board of both teams.

        Arguments:
            None.
            
        Returns:
            None.
    
        """
        
        self.builder.get_object("scoreboard_mad").set_text(str(self.score_mad))
        self.builder.get_object("scoreboard_atl").set_text(str(self.score_atl))
        
        return
    
    def update_remaining_balls(self):
        """
        Description:
            This function updates the images from the remaining balls 
            during the match. If the score is 3-3, the last ball is shown red.

        Arguments:
            None.
            
        Returns:
            None.
    
        """
        
        for i in range(7):
            self.builder.get_object("ball_%d" % (i+1)).set_from_file("images/football_active.png")
        
        for i in range(self.score_mad + self.score_atl + self.score_reset):
            self.builder.get_object("ball_%d" % (i+1)).set_from_file("images/football_inactive.png")
        
        if self.score_mad == 3 and self.score_atl == 3:
            self.builder.get_object("ball_7").set_from_file("images/football_tradition.png")
        
        return
    
    def update_goal_dash(self):
        """
        Description:
            This function updates the goal dash of both teams.
            The goal dash is the text box where the description
            of the goals is shown.

        Arguments:
            None.
            
        Returns:
            None.
    
        """
        
        # Update goal dash of Madrid
        
        # TODO: Change this to a list and then join to form a string
        
        dash_text = ""
        
        for goal in self.goals_mad:
            
            goal_time       = goal[1]
            figure          = goal[2]
            legal           = goal[3]
            player_nickname = goal[4]                        
                    
            dash_text += "%02d:%02d - %-15s %4s" % (goal_time/60, goal_time%60, player_nickname, '('+str((figure-1)%11+1)+')')
                    
            if (not legal) and figure == 20:
                dash_text += "(G)"
            elif (not legal) and figure in (15, 16, 17, 18, 19):
                dash_text += "(M)"
            elif legal and (12 <= figure <= 22):
                dash_text += "(P)"
                
            dash_text += "\n"
            
        self.builder.get_object("goals_mad_txt").set_text(dash_text)
        
        # Update goal dash of Atletico
        
        dash_text = ""
        
        for goal in self.goals_atl:
            
            goal_time       = goal[1]
            figure          = goal[2]
            legal           = goal[3]
            player_nickname = goal[4]                        
                    
            dash_text += "%02d:%02d - %-15s %4s" % (goal_time/60, goal_time%60, player_nickname, '('+str((figure-1)%11+1)+')')
                    
            if (not legal) and figure == 9:
                dash_text += "(G)"
            elif (not legal) and figure in (4, 5, 6, 7, 8):
                dash_text += "(M)"
            elif legal and (1 <= figure <= 11):
                dash_text += "(P)"
                
            dash_text += "\n"
            
        self.builder.get_object("goals_atl_txt").set_text(dash_text)
        
        return
    
    def update_team_rows_characters(self):
        """
        Description:
            This function updates the characters shown in the row of 
            the select team screen, within a new championship.
            If the character has already been selected, it is grayed out.

        Arguments:
            None.
            
        Returns:
            None.
    
        """
        
        self.ch_first_row = self.ch_order[(self.current_row - 1) * 8: self.current_row * 8]
        self.ch_second_row = self.ch_order[self.current_row * 8: (self.current_row + 1) * 8]
                
        for i in range(len(self.ch_first_row)):
            
            ch = self.builder.get_object("team_ch_%d" % (i+1))
            
            ch_pid = self.ch_first_row[i]
            
            ch.set_from_file("images/characters/ch_%d.png" % ch_pid)
            
            self.builder.get_object("ebox_team_ch_%d" % (i+1)).show()
            
            # Check if character image should be grayed out
                        
            taken_pids = []
            
            for j in range(len(self.championship_teams)):
                if j != (self.last_team_selected - 1):
                    taken_pids += self.championship_teams[j]
                                            
            if (([ch_pid] in self.team_selected_players) or (ch_pid in taken_pids)) and ch_pid != 0:
                                        
                gray_ch = GdkPixbuf.Pixbuf.new_from_file('images/characters/ch_%d.png' % ch_pid)
                gray_ch.saturate_and_pixelate(gray_ch, 1.0, 0.5)
    
                ch.set_from_pixbuf(gray_ch)
            
        for i in range(len(self.ch_first_row), 8):
            self.builder.get_object("ebox_team_ch_%d" % (i+1)).hide() 
                
        return
    
    def update_championship_matches(self):
        """
        Description:
            This function updates the characters shown in the championship
            matches screen. If the match has already been played, it is grayed out.

        Arguments:
            None.
            
        Returns:
            None.
    
        """
        
        for i in range(len(self.current_championship.matches)):
            
            if self.current_championship.matches[i] == [-1, -1, -1, -1, -1, -1]: continue # Match not played
            
            # For the two teams of each match
                        
            for j in range(2):
                
                ch_1, ch_2 = self.current_championship.matches[i][j*2:(j+1)*2]
                
                if (ch_1, ch_2) == (-1, -1): continue
                
                small_ch_1 = GdkPixbuf.Pixbuf.new_from_file_at_size('images/characters/ch_%d.png' % ch_1, 63, 78)
                small_ch_2 = GdkPixbuf.Pixbuf.new_from_file_at_size('images/characters/ch_%d.png' % ch_2, 63, 78)
                
                # Change to grey scale if match already played
                
                if self.current_championship.matches[i][4:6] != [-1, -1]:
                    
                    small_ch_1.saturate_and_pixelate(small_ch_1, 1.0, 0.5)
                    small_ch_2.saturate_and_pixelate(small_ch_2, 1.0, 0.5)
                    
                self.builder.get_object("b_m_player_1_team_%d" % (i*2+1+j)).set_from_pixbuf(small_ch_1)
                self.builder.get_object("b_m_player_2_team_%d" % (i*2+1+j)).set_from_pixbuf(small_ch_2)
        
        return
    
    
    " *** General Functions *** "
    
    def set_ch_order(self):
        """
        Description:
            This function reads the config.ini file and decides if the order of
            the characters has to be updated. The update is done once a day.
            The date of the last update is saved in the same file.
            The order of the characters is decided taking into account the days since
            the last match played and the total amount of matches played in the
            current season. Both parameters have a 50% of weight in the final coefficient.

        Arguments:
            None.
            
        Returns:
            None.
    
        """
        
        config = ConfigParser.SafeConfigParser()
        config.read("config.ini")
        ch_order_last_updated = config.get("Section ch_order", "ch_order_last_updated")
        
        ch_order_last_updated = datetime.date(*[int(i) for i in ch_order_last_updated.split('-')])

        # If ch_order was not calculated today, calculate it and update the config.ini file
        
        if ch_order_last_updated != datetime.date.today():
                    
            self.logger.info("Calculating new characters order...")
                    
            self.ch_order = []
            
            # Get total number of players stored in DB
            
            n_total_players = db_handler.get_n_players()
            
            # Add only the ones that are active
            
            for i in range(n_total_players):
                if db_handler.get_player(i).active:
                    self.ch_order.append(i)
            
            # Get the parameters to sort ch_order (n_matches and last_match_date)
            
            ch_order_parameters = []
                    
            for i in range(len(self.ch_order)):
                ch_order_parameters.append([0,0])
                ch_order_parameters[i][0] = db_handler.get_n_matches_last_season(self.ch_order[i])
                ch_order_parameters[i][1] = db_handler.get_days_since_last_match(self.ch_order[i])            
                    
            # Sort ch_order depending on the n_matches and last_match_date
                    
            self.ch_order = self.sort_ch_order(ch_order_parameters)
            
            # Put PID = 0 (Anonimo) in first place, always.
            
            self.ch_order.remove(0)
            self.ch_order.insert(0, 0)   
            
            # Update the ch_order_last_updated date in the config.ini file
            config.set("Section ch_order", "ch_order_last_updated", str(datetime.date.today()))
            
            # Update the ch_order in the config.ini file
            config.set("Section ch_order", "ch_order", str(self.ch_order)[1:-1])
            
            with open("config.ini", "w") as configfile:
                config.write(configfile)
                
            self.logger.info("New characters order calculated.")
                    
        # If ch_order was already calculated today, read it from the config.ini file
        
        else:
            self.logger.info("Getting characters order from config file.")
            
            ch_order_config = config.get("Section ch_order", "ch_order")
            
            self.ch_order = [int(i) for i in ch_order_config.split(',')]
            
        
        # Check for differences between the PIDs in ch_order and in the DB
        
        players_pids = db_handler.get_active_players_pids()
            
        for pid in self.ch_order:
            if pid not in players_pids:
                self.ch_order.remove(pid)
                
        for pid in players_pids:
            if pid not in self.ch_order:
                self.ch_order.append(pid)
                
        return
    
    def sort_ch_order(self, parameters):
        """
        Description:
            This function sorts the order of the characters depending on the
            values of the list parameters.

        Arguments:
            parameters (list): This list contains two values for each of the 
                               characters involved. The first value corresponds
                               to the number of matches in the current season
                               while the second value is the amount of days since
                               the last match played.
            
        Returns:
            list: Ordered version of ch_order
    
        """
        
        indexes = [0] * len(self.ch_order)
        
        # Get the total number of matches of last season
        
        total_matches = db_handler.get_n_matches_last_season(-1)
        
        for i in range(len(self.ch_order)):
            
            # Add depending on the number of matches in last season
                        
            indexes[i] += (parameters[i][0] * 0.5) / total_matches
                        
            # Add depending on the days since last match
                        
            if parameters[i][1] != -1:
                indexes[i] += max(0, (10 - parameters[i][1]) * 0.05)
                                      
        return [ch for (i,ch) in sorted(zip(indexes, self.ch_order), reverse=True)]
    
    def initialize_game(self, keep="none"):
        """
        Description:
            This function initializes the game setup, and is usually done after
            showing the characters screen.
            The function updates the players images, the teams logos, initializes
            some of the match variables and updates the characters rows.

        Arguments:
            keep (string): Defines which players should remain selected. The possible 
                           values should be "none", "winner" or "both".
            
        Returns:
            None.
    
        """
        
        # Restart players images 
        
        if keep == "none":
            for i in range(4):
                self.builder.get_object("player_%s" % (i+1)).set_from_file("images/player_%s.png" % (i+1))
            
            self.selected_players = [[], [], [], []]
            self.selected_players_names = ["", "", "", ""]
            
        elif keep == "winner":
            if self.score_mad > self.score_atl:
                self.builder.get_object("player_3").set_from_file("images/player_3.png")
                self.builder.get_object("player_4").set_from_file("images/player_4.png")
                
                self.selected_players[2] = []
                self.selected_players[3] = []
                
                self.selected_players_names[2] = ""
                self.selected_players_names[3] = ""
                
            elif self.score_atl > self.score_mad:
                self.builder.get_object("player_1").set_from_file("images/player_1.png")
                self.builder.get_object("player_2").set_from_file("images/player_2.png")
                
                self.selected_players[0] = []
                self.selected_players[1] = []
                
                self.selected_players_names[0] = ""
                self.selected_players_names[1] = ""
                
        elif keep == "both":
            pass
                
        # Restart team logos
            
        self.check_modes()
            
        # Initialize variables
            
        self.last_ch_clicked = -1
        
        self.current_row = 1
        self.ch_first_row  = self.ch_order[0:8]
        self.ch_second_row = self.ch_order[8:16]
        
        self.saving = False
        
        self.championship_match = False
        
        self.start_released = False
        
        self.mundialito_matches = []
        self.mundialito_button_clicked = -1
        self.mundialito_pattern_correct = False
        
        # Update characters rows
        
        self.update_rows_characters()     
                            
        return
    
    def initialize_championship_game(self):
        """
        Description:
            This function initializes a championship game. It sets the character 
            images for each player, it updates the teams logos and initializes the
            variables for the match.

        Arguments:
            None.
            
        Returns:
            None.
    
        """
                
        # Set the players for the match
        
        self.selected_players = [[pid] for pid in self.current_championship.matches[self.championship_match_n - 1][0:4]]
                
        # Set players images for the match
        
        for i in range(4):
            self.builder.get_object("player_%d" % (i+1)).set_from_file("images/characters/ch_%d.png" % self.selected_players[i][0])
                
        # Restart team logos
            
        self.check_modes()
            
        # Initialize variables
            
        self.last_team_ch_clicked = -1
                
        self.score_mad = 0
        self.score_atl = 0
        self.score_reset = 0
        self.goals_mad = []
        self.goals_atl = []
        
        self.match_time = 0
        
        self.championship_match = True     
        
        self.saving = False
                            
        return
    
    def check_modes(self):
        """
        Description:
            This function checks if the selected players compose a particular team.
            If they do, the team logo is changed to their particular team image.

        Arguments:
            None.
            
        Returns:
            None.
    
        """
        
        # Check modes in Madrid
        
        if [2] in self.selected_players[0:2]:
            self.builder.get_object("logo_mad").set_from_file("images/logo_getafe.png")
                    
        elif [40] in self.selected_players[0:2] and [42] in self.selected_players[0:2]:
            self.builder.get_object("logo_mad").set_from_file("images/logo_yeti.png")
            
        elif [5] in self.selected_players[0:2] and [36] in self.selected_players[0:2]:
            self.builder.get_object("logo_mad").set_from_file("images/logo_tonadi.png")
            
        elif [3] in self.selected_players[0:2] and [9] in self.selected_players[0:2]:
            self.builder.get_object("logo_mad").set_from_file("images/logo_churreros.png")
            
        elif [9] in self.selected_players[0:2] and [24] in self.selected_players[0:2]:
            self.builder.get_object("logo_mad").set_from_file("images/logo_b104killers.png")
            
        else:
            self.builder.get_object("logo_mad").set_from_file("images/logo_mad.png")
        
        # Check modes in Atletico
        
        if [2] in self.selected_players[2:4]:
            self.builder.get_object("logo_atl").set_from_file("images/logo_getafe.png")
                    
        elif [40] in self.selected_players[2:4] and [42] in self.selected_players[2:4]:
            self.builder.get_object("logo_atl").set_from_file("images/logo_yeti.png")
            
        elif [5] in self.selected_players[2:4] and [36] in self.selected_players[2:4]:
            self.builder.get_object("logo_atl").set_from_file("images/logo_tonadi.png")
            
        elif [3] in self.selected_players[2:4] and [9] in self.selected_players[2:4]:
            self.builder.get_object("logo_atl").set_from_file("images/logo_churreros.png")
            
        elif [9] in self.selected_players[2:4] and [24] in self.selected_players[2:4]:
            self.builder.get_object("logo_atl").set_from_file("images/logo_b104killers.png")
        
        else:
            self.builder.get_object("logo_atl").set_from_file("images/logo_atl.png")
            
        return
    
    def backup_database(self):
        """
        Description:
            This function creates a backup of the database once a day. 
            The date of the last backup is stored in the config.ini file.
            The backup file is stored in the directory /db/backups/ and
            the name of the file has the date at the end.

        Arguments:
            None.
            
        Returns:
            None.
    
        """
        
        if 'dev' in sys.argv:
            return
        
        config = ConfigParser.SafeConfigParser()
        config.read("config.ini")
        
        last_backup = config.get("Section backup", "last_backup")
        
        last_backup = datetime.date(*[int(i) for i in last_backup.split('-')])
        
        if last_backup != datetime.date.today():
            
            self.logger.info("Making backup of database...")
            
            # Make backup
            
            shutil.copyfile("db/futbolin.db", "db/backups/futbolin_%s.db" % datetime.date.today())
            
            # Update the last_updated date in the config.ini file
            
            config.set("Section backup", "last_backup", str(datetime.date.today()))
            
            # Write changes in config.ini
            
            with open("config.ini", "w") as configfile:
                config.write(configfile)
                
            self.logger.info("Backup done.")
            
        return
    
    def play_match_sounds(self):
        """
        Description:
            This function plays ambient sounds during a match. It selects
            a sound file at random and then calls the aplay command.
            The ambient_0 sound is played first because it has an initial 
            whistle. While the match is being played, the sounds keep playing
            in a while loop.

        Arguments:
            None.
    
        Returns:
            None.
    
        """
            
        if 'dev' in sys.argv:
            return
        
        # Use aplay to play sound
        
        ambient_sounds_list = ["ambient_%d.wav" % i for i in range(1, 15)]
                
        # ambient_0.wav has the initial whistle
                            
        subprocess.call(["aplay", "sounds/ambient_0.wav"])
                
        while self.in_match:
                        
            sound_file_name = random.choice(ambient_sounds_list)
            
            subprocess.call(["aplay", "sounds/%s" % sound_file_name])
            
        return
    
    def play_sound(self, file_name):
        """
        Description:
            This function plays a sound by launching a thread with the aplay
            command and the file name passed as argument.

        Arguments:
            file_name (String): File name of the sound to play.
    
        Returns:
            None.
    
        """
                       
        if 'dev' in sys.argv:
            return
        
        # Use aplay to play sound
        
        self.start_thread(subprocess.call, (["aplay", "sounds/%s" % file_name],))
                    
        return
    
    def initialize_logger(self):
        """
        Description:
            This function initializes the logger and directs all the exception errors
            to the write to logger function.

        Arguments:
            None.
    
        Returns:
            logger.
    
        """
                
        logger = logging.getLogger(__name__)
        logger.setLevel(logging.INFO)
                        
        # Set max log file size to 1 GiB
                
        handler = handlers.RotatingFileHandler('futbolin.log', mode='a', maxBytes=1073741824)
        handler.setLevel(logging.INFO)
        
        formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
        handler.setFormatter(formatter)
        
        logger.addHandler(handler)
        
        # Send all uncaught exceptions to own function
        
        if 'disable_log' not in sys.argv:
            sys.excepthook = self.write_exception_to_log
        
        return logger
    
    def write_exception_to_log(self, e_type, e_value, e_tb):
        """
        Description:
            This function catches all the exceptions and writes them to the
            logger.

        Arguments:
            None.
    
        Returns:
            None.
    
        """
        
        self.logger.exception("Uncaught exception: {0}".format(str(e_value)))
        
        if 'dev' in sys.argv:
            print "Uncaught exception:", e_value
        
    def show_webcam_snapshot(self):
        """
        Description:
            This function accesses the webcam web service and retrieves the snapshot.
            If successful, it sets the image to the object of the GUI.

        Arguments:
            None.
    
        Returns:
            None.
    
        """
        
        try:
            # Get and save Image
        
            urllib.urlretrieve("http://cam-b105.lsi.die/jpg/image.jpg", "images/snapshot.jpg")

        except Exception as e:
            
            self.logger.info("Error when updating webcam snapshot: %s." % e.message)
            
        try:
            # Set image instead of stats
            
            Gdk.threads_enter()
            
            stats_img = self.builder.get_object("webcam_snapshot")
            
            snapshot_img = GdkPixbuf.Pixbuf.new_from_file_at_size('images/snapshot.jpg', 320, 240)
                    
            stats_img.set_from_pixbuf(snapshot_img)
            
            Gdk.threads_leave()
            
        except Exception as e:
            
            self.logger.info("Error when updating webcam snapshot: %s." % e.message)
            
            self.builder.get_object("webcam_snapshot").set_from_file('images/snapshot_default.png')
            
            Gdk.threads_leave()
            
        return
    
    
    " *** Threading Functions *** "

    def start_thread(self, thr_function, arguments=None):
        """
        Description:
            This function handles the launching of a new thread.

        Arguments:
            thr_function (function)
            arguments (tuple)
            
        Returns:
            None.
    
        """
        
        if arguments == None:
            my_thread = threading.Thread(target=thr_function)
            
        else:
            my_thread = threading.Thread(target=thr_function, args=arguments)
                
        my_thread.daemon = True
        
        my_thread.start()
        
        return

    def animate_start_button(self):                
        """
        Description:
            This function animates the start match button by hiding and showing
            an object of the GUI every half a second. This function should be
            executed within a thread. The thread will die when the current screen
            is different from characters of championship match.

        Arguments:
            None.
            
        Returns:
            None.
    
        """
           
        while self.current_screen == "characters" or self.current_screen == "championship_match":                        
            
            sleep(0.5)
            
            Gdk.threads_enter()
            self.builder.get_object("b_start").hide()
            Gdk.threads_leave()
            
            sleep(0.5)
            
            Gdk.threads_enter()
            self.builder.get_object("b_start").show()
            Gdk.threads_leave()
            
        return

    def count_time(self):
        """
        Description:
            This function is in charge of keeping track of the time within the application.
            Not only it is used for the match time but also for the timeouts.
            The function also starts a thread to show a snapshot of the webcam every second.
            The function also updates the string of the time board and hides the match 
            stats 15 seconds after the beginning of the match or a goal.

        Arguments:
            None.
            
        Returns:
            None.
    
        """
        
        while self.in_match:                        
            
            if self.current_screen == 'match':
                
                # Get GTK lock before modifying the GUI
                
                Gdk.threads_enter()
                
                self.builder.get_object("timeboard").set_text("%02d : %02d" % (self.match_time/60, self.match_time % 60))
                
                self.start_thread(self.show_webcam_snapshot, ())
                
                self.builder.get_object("stats_match_bkg").show()
                
                if self.match_time - self.last_goal_time > 15:
                    self.builder.get_object("stats_match_bkg").hide()
                    self.builder.get_object("bargraph_base").hide()
                    for i in range(12):
                        self.builder.get_object("bargraph_%d" % (i+1)).hide()
                        
                    self.builder.get_object("stats_match_description").hide()
                    self.builder.get_object("stats_match_nickname").hide()
                    self.builder.get_object("stats_match_data").hide()            
                
                Gdk.threads_leave()
                                                
                sleep(1) 
                
                self.match_time += 1
                
                
            elif self.current_screen == 'ask_scorer' or self.current_screen == 'ask_legal':
                
                sleep(1)
                
                self.ask_timeout -= 1
                
                if self.ask_timeout < 0:
                    
                    if self.current_screen == 'ask_scorer':
                        self.b_cross_clicked(None)
                        
                    elif self.current_screen == 'ask_legal':
                        self.b_legal_yes_clicked(None)   
                
                # Get GTK lock before modifying the GUI
                
                Gdk.threads_enter()
                
                self.builder.get_object("legal_timeout").set_text(str(self.ask_timeout))                                        
                
                Gdk.threads_leave()
                                                                  
        return
    
    
    " *** Characters Screen - Buttons Handlers *** "

    def b_start_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when the start match button is clicked.
            If there is a player not yet selected, the function returns immediately.
            The match variables are initialized, a thread to count time is launched 
            and another thread is launched to play the match sounds.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
               
        # Ignore if not all players were selected
        
        if [] in self.selected_players:
            self.play_sound("error.wav")
            return
               
        # Check mundialito mode
        
        if self.mundialito_mode and len(self.mundialito_matches) == 0:
            
            # Make the matches combination
            
            self.mundialito_matches.append([self.selected_players[0][:], self.selected_players[1][:], self.selected_players[2][:], self.selected_players[3][:]])
                            
            self.mundialito_matches.append([self.selected_players[0][:], self.selected_players[1][:], self.selected_players[3][:], self.selected_players[2][:]])
            
            self.mundialito_matches.append([self.selected_players[1][:], self.selected_players[0][:], self.selected_players[2][:], self.selected_players[3][:]])
            
            self.mundialito_matches.append([self.selected_players[1][:], self.selected_players[0][:], self.selected_players[3][:], self.selected_players[2][:]])
            
            # Set initial message for mundialito message
            
            self.builder.get_object("mundialito_msg").set_text("Mundialito 1/4")
            
            # Set the background image for mundialito mode
            
            if '169' in sys.argv:
                self.builder.get_object("background").set_from_file("images/background_mundialito.jpg")
            else:
                self.builder.get_object("background").set_from_file("images/background_mundialito_5_4.jpg")
                
        # Initialize Match variables
        
        self.score_mad = 0
        self.score_atl = 0
        self.score_reset = 0
        self.goals_mad = []
        self.goals_atl = []
        self.match_time = 0
        self.last_goal_time = 0
                
        # Get players names
        
        self.get_players_names()                                

        # Update the in_match flag
               
        self.update_match_status()
                                                                                  
        # Set match start time
        
        self.start_time = time.strftime("%H:%M:%S")
                                        
        # Launch thread to keep track of time
                        
        self.start_thread(self.count_time)
        
        # Show Match widgets
        
        self.builder.get_object("timeboard").set_text("%02d : %02d" % (self.match_time/60, self.match_time % 60))
        
        self.show_match_screen()
        
        # Start thread to play match sounds
        
        self.start_thread(self.play_match_sounds, ())
        
        return
    
    def b_exit_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when the exit button is clicked.
            The user is prompted with the ask confirmation screen before
            any action is taken.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
        
        self.show_ask_confirmation_screen()
        
        return
        
    def b_player_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when one of the four players in the characters
            screen is clicked. If not in the characters screen, the function returns
            immediately. If no character has been selected previously, the function
            also returns immediately.
            A sound is played when the new character is selected into one of the players.
            If the character was already in one of the players slots, it is removed from
            the old slot and placed in the new one. The GUI object's name is used to 
            identify which one of the four players was clicked.
            This function also calls a function to check for team modes.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
        
        if self.current_screen != "characters": return
        
        if self.last_ch_clicked == -1: return
        
        # Play select sound
        
        self.play_sound("select_1.wav")
        
        if self.last_ch_clicked != 0:
            
            # Remove character from old player if already selected

            if ([self.last_ch_clicked] in self.selected_players):
                old_player_n = self.selected_players.index([self.last_ch_clicked]) + 1
                old_pl = self.builder.get_object("player_%s" % old_player_n)
                old_pl.set_from_file("images/player_%s.png" % old_player_n)
                self.selected_players[old_player_n - 1] = []
                
        # Set the new player with this character
        
        player_n = int(obj.get_name())
            
        pl = self.builder.get_object("player_%d" % player_n)
        
        pl.set_from_file("images/characters/ch_%d.png" % self.last_ch_clicked)
        
        self.selected_players[player_n - 1] = [self.last_ch_clicked]
        
        # Check Modes 
        
        self.check_modes()
        
        return
    
    def b_ch_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when a character is selected from any of the 
            two rows of characters. The character's pid is saved in the class variable
            last_ch_clicked. The GUI object's name is used to identify which one of the
            16 characters was clicked.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
                        
        ch_n = int(obj.get_name())
                                
        if 1 <= ch_n <= 8 and len(self.ch_first_row) >= ch_n:
            self.last_ch_clicked = self.ch_first_row[ch_n - 1]
                
        elif 9 <= ch_n <= 16 and len(self.ch_second_row) >= (ch_n - 8):
            self.last_ch_clicked = self.ch_second_row[ch_n - 9]
            
        return
       
    def b_mad_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when the team logo is clicked. If in the
            characters screen, the action is to swap the characters from defense
            to attack in the Madrid team. If in the match screen, the action is
            to record a goal done by Madrid. If in the match screen but the match
            is over, the function returns immediately.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
        
        if self.current_screen == "match" and self.in_match:
            self.mad_goal()
            return
        
        elif self.current_screen == "match" and not self.in_match:
            return
                        
        # Toggle the characters in team Madrid 
        
        self.selected_players[0], self.selected_players[1] = self.selected_players[1], self.selected_players[0]
        
        self.pl_1 = self.builder.get_object("player_1")        
        if self.selected_players[0] != []:            
            self.pl_1.set_from_file("images/characters/ch_%d.png" % self.selected_players[0][0])
        else:
            self.pl_1.set_from_file("images/player_1.png")
            
        self.pl_2 = self.builder.get_object("player_2")            
        if self.selected_players[1] != []:            
            self.pl_2.set_from_file("images/characters/ch_%d.png" % self.selected_players[1][0])
        else:
            self.pl_2.set_from_file("images/player_2.png")        
        
        return
    
    def b_atl_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when the team logo is clicked. If in the
            characters screen, the action is to swap the characters from defense
            to attack in the Atletico team. If in the match screen, the action is
            to record a goal done by Atletico. If in the match screen but the match
            is over, the function returns immediately.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
        
        if self.current_screen == "match" and self.in_match:
            self.atl_goal()
            return
        
        elif self.current_screen == "match" and not self.in_match:
            return
                            
        # Toggle the characters in team Atletico
        
        self.selected_players[2], self.selected_players[3] = self.selected_players[3], self.selected_players[2]
        
        self.pl_3 = self.builder.get_object("player_3")        
        if self.selected_players[2] != []:            
            self.pl_3.set_from_file("images/characters/ch_%d.png" % self.selected_players[2][0])
        else:
            self.pl_3.set_from_file("images/player_3.png")
            
        self.pl_4 = self.builder.get_object("player_4")            
        if self.selected_players[3] != []:            
            self.pl_4.set_from_file("images/characters/ch_%d.png" % self.selected_players[3][0])
        else:
            self.pl_4.set_from_file("images/player_4.png")        
        
        return
       
    def b_up_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when the up arrow button is clicked.
            The class variable current_row is decreased and the new characters
            are updated into the lists. If in the first or last row, the
            corresponding buttons are shown or hidden.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
        
        # Substract 8 to elements of the ch_first_line list if not in the first row 
        if self.current_row != 1:
            self.current_row -= 1           
        
        # Hide up button if already in the first row
        if self.current_row == 1:
            self.builder.get_object("ebox_b_up").hide()
            
        # Show down button if not in the last row
        if self.current_row != self.last_row:
            self.builder.get_object("ebox_b_down").show()
        
        self.update_rows_characters()
        
        return          
        
    def b_down_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when the down arrow button is clicked.
            The class variable current_row is increased and the new characters
            are updated into the lists. If in the first or last row, the
            corresponding buttons are shown or hidden.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
                
        # Add 8 to elements of the ch_first_line list if not in the last row
        if self.current_row != self.last_row:
            self.current_row += 1       
        
        # Hide down button if already in the last row            
        if self.current_row == self.last_row:
            self.builder.get_object("ebox_b_down").hide()
            
        # Show up button if not in the first row
        if self.current_row != 1:
            self.builder.get_object("ebox_b_up").show()                          
                    
        self.update_rows_characters()
        
        return    
    
    def b_main_menu_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when the main menu button is clicked.
            It shows the main menu screen.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
        
        self.show_main_menu_screen()
            
        return
      
    def b_mundialito_1_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when the mundialito button at the top left
            corner is clicked. This is the first button of the sequence to activate
            the mundialito mode.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
                
        self.play_sound("select_1.wav")
        
        self.mundialito_button_clicked = 1
        
        self.mundialito_pattern_correct = True
        
        self.builder.get_object("mundialito_1").show()
        
        return
    
    def b_mundialito_2_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when the mundialito button at the top right
            corner is clicked. This is the second button of the sequence to activate
            the mundialito mode.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
                
        if not (self.mundialito_pattern_correct and self.mundialito_button_clicked == 1):
            self.mundialito_pattern_correct = False
            
            for i in range(1, 5):
                self.builder.get_object("mundialito_%d" % i).hide()
            
        else:
            self.play_sound("select_1.wav")
            
            self.builder.get_object("mundialito_2").show()
        
        self.mundialito_button_clicked = 2
        
        return
    
    def b_mundialito_3_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when the mundialito button at the bottom right
            corner is clicked. This is the third button of the sequence to activate
            the mundialito mode.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
              
        if not (self.mundialito_pattern_correct and self.mundialito_button_clicked == 2):
            self.mundialito_pattern_correct = False
            
            for i in range(1, 5):
                self.builder.get_object("mundialito_%d" % i).hide()
            
        else:
            self.play_sound("select_1.wav")
            
            self.builder.get_object("mundialito_3").show()
        
        self.mundialito_button_clicked = 3
        
        return
    
    def b_mundialito_4_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when the mundialito button at the bottom left
            corner is clicked. This is the fourth button of the sequence to activate
            the mundialito mode.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """     
          
        if not (self.mundialito_pattern_correct and self.mundialito_button_clicked == 3):
            self.mundialito_pattern_correct = False
            
            for i in range(1, 5):
                self.builder.get_object("mundialito_%d" % i).hide()
            
        else:
            self.mundialito_mode = True
            
            self.play_sound("select_1.wav")
            
            self.builder.get_object("mundialito_4").show()
                                    
        self.mundialito_button_clicked = 4
        
        return
    
    
    " *** Characters Screen - Functions *** "
    
    def get_match_stats(self):
        """
        Description:
            This function handles the match stats by taking information from
            the database. It retrieves the results of all the matches between
            the teams involved and groups them into the possible results. It 
            also sets the height for the bars of the bargraph.

        Arguments:
            None.
    
        Returns:
            None.
    
        """
        
        # Get matches between from DB
        
        players_pids = [i[0] for i in self.selected_players]
        
        matches_between = db_handler.get_matches_between(*players_pids)
                
        n_matches_between = sum(matches_between)
                
        for i in range(12):
            
            bar_object = self.builder.get_object("bargraph_%d" % (i+1))  
            
            if n_matches_between == 0:
                bar_object.hide()
                continue         
                        
            max_height = (150 * 150) / (max(matches_between) * 150.0 / n_matches_between)
            
            # Make sure that bar_height is > 0
            
            bar_height = max(1, matches_between[i] * max_height / n_matches_between)
                        
            bar_image = GdkPixbuf.Pixbuf.new_from_file('images/bargraph_%d.png' % (i/6+1))
                        
            bar_image = bar_image.scale_simple(12, bar_height, GdkPixbuf.InterpType.BILINEAR) 
                           
            # Move new bar downwards
            
            new_y = 359 + (150 - bar_height)
            old_x = 503 + (i*24)
            
            # Adapt drawings to 5:4 screen
            
            if self.screen_ratio == 54: new_y += 152            
            
            self.builder.get_object("layout1").move(bar_object, old_x, new_y)
                           
            bar_object.set_from_pixbuf(bar_image)
                                                                  
        return
    
    def get_players_names(self):
        """
        Description:
            This function gets the nicknames of the players involved in the
            actual match. The information is retrieved from the database.

        Arguments:
            None.
    
        Returns:
            None.
    
        """
        
        for i in range(4):
            player = db_handler.get_player(self.selected_players[i][0])
            self.selected_players_names[i] = player.nickname
            
        return
    
    def update_match_status(self):
        """
        Description:
            This function updates the status of the match and saves it in the
            class variable in_match. If the match is still on, the value is True.
            If the match is in pause or finished, the value is False.

        Arguments:
            None.
    
        Returns:
            None.
    
        """
        
        if (self.score_mad + self.score_atl == 7):
            self.in_match = False
        
        elif (self.score_mad >= 4 and self.score_atl != 0):
            self.in_match = False
            
        elif (self.score_atl >= 4 and self.score_mad != 0):
            self.in_match = False
            
        else:
            self.in_match = True
            
        if self.score_reset > 0:
            
            if (self.score_mad + self.score_atl == (7 - self.score_reset)):
                self.in_match = False
                
            elif (self.score_mad >= (7 - self.score_reset)/2 + 1) and self.score_atl != 0:
                self.in_match = False
                
            elif (self.score_atl >= (7 - self.score_reset)/2 + 1) and self.score_mad != 0:
                self.in_match = False
                
            else:
                self.in_match = True
            
        return
    
    
    " *** Ask Quit Question Screen - Buttons Handlers *** "
    
    def b_confirmation_yes_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when the YES button is clicked when the 
            user is prompted with the confirmation question screen.
            Depending on the current screen, the action taken differs. If the 
            current screen is the match screen, the action also depends on the
            last clicked button, which can be either Cancel or Reset.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
        
        if self.current_screen == "characters":
            self.window.destroy()
            Gtk.main_quit()
            
        elif self.current_screen == "match":
            
            if self.match_b_clicked == "cancel":
                self.in_match = False
            
                self.mundialito_mode = False
                
                # If in championship, return to championship screen
                
                if self.championship_match:
                    self.show_championship_matches_screen()
                
                else:                            
                    # Show characters screen
                                    
                    self.show_characters_screen()
                    
                    # Initialize new game
                    
                    self.initialize_game(keep="both")
                    
            elif self.match_b_clicked == "reset":
                
                self.score_reset = self.score_mad + self.score_atl + self.score_reset
        
                self.score_mad = 0
                self.score_atl = 0
                
                self.goals_mad = []
                self.goals_atl = []
                
                self.update_match_status()
                
                self.show_match_screen()
                
                self.start_thread(self.count_time, ()) # This has to check if the thread actually died (one second had to pass)
                
        elif self.current_screen == "championships":
            
            self.start_new_championship()
        
        return
    
    def b_confirmation_no_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when the NO button is clicked when the 
            user is prompted with the confirmation question screen.
            Depending on the current screen, the action taken differs. If the 
            current screen is the match screen, the action also depends on the
            last clicked button, which can be either Cancel or Reset.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
        
        if self.current_screen == "characters":
            
            self.show_characters_screen()
        
        elif self.current_screen == "match":
            
            self.update_match_status()
            
            self.show_match_screen()
            
            if self.in_match:
                
                sleep(1) # TOERASE # This way we are sure that the count_time thread was killed when in_match was set to False
                
                self.start_thread(self.count_time, ())
                
        elif self.current_screen == "championships":
            
            self.show_championships_screen()
        
        return
    
    
    " *** Main Menu Screen - Buttons Handlers *** "
    
    def b_championships_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when the Championship button is clicked
            within the Main Menu. It first gets the data from the last championship
            stored in the database and then it shows the championship screen.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
        
        # Get last championship from DB
        
        self.current_championship = db_handler.get_championship()
        
        # Show Championships screen
        
        self.show_championships_screen()
        
        return
    
    def b_seasons_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when the Seasons button is clicked
            within the Main Menu. It first gets the sid of the last season from
            the database, shows the season screen and then calls a function
            to show the season stats.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
        
        self.current_season_sid = db_handler.get_last_sid() + 1
        
        self.sid_shown = self.current_season_sid
        
        # Show Seasons screen
        
        self.show_seasons_screen()
            
        # Show season stats
        
        self.show_season_stats()
        
        # Hide seasons right button
        
        self.builder.get_object("ebox_b_seasons_right").hide()
        
        return
    
    def b_stats_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when the Statistics button is clicked
            within the Main Menu. It sets the stats_ch_i to the first character,
            skipping ch zero (anonymous). It updates the stats texts for that
            character and finally shows the stats screen.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
        
        self.stats_ch_i = 1     # Skips anonymous
        
        self.update_ch_stats()
        
        # Show stats screen
        
        self.show_stats_screen()
        
        return
       
    def b_back_main_menu_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when the back button is clicked within the 
            main menu and its submenues. Depending on the current screen, a different
            screen is shown. 

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
        
        if self.current_screen == "main_menu":
            self.initialize_game()
            self.show_characters_screen()
        
        elif self.current_screen == "championships":            
            self.show_main_menu_screen()
            
        elif self.current_screen == "new_championship":
            self.show_championships_screen()
            
        elif self.current_screen == "select_team":
            self.show_new_championship_screen()
            
        elif self.current_screen == "championship_matches":
            self.show_championships_screen()
            
        elif self.current_screen == "championship_match":
            self.show_championship_matches_screen()
            
        elif self.current_screen == "congratulations":
            self.initialize_game()
            self.show_characters_screen()
            
        elif self.current_screen == "seasons":
            self.show_main_menu_screen()
            
        elif self.current_screen == "stats":
            self.show_main_menu_screen()
        
        return
    
        
    " *** Championships Screen - Buttons Handlers *** "
    
    def b_continue_championship_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when the continue championship button is
            clicked. It shows the championship matches screen.
            This button is shown only if last championship is not finished yet.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
        
        # Get teams from last championship
        
        self.championship_teams = self.current_championship.teams
        
        # Show the championship matches
                
        self.show_championship_matches_screen()
        
        return
    
    def b_new_championship_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when the new championship button is
            clicked. If the current championship is not finished, it prompts the
            user with the ask confirmation screen first. If positive, it shows
            the new championship screen.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
        
        # Check if there is an unfinished championship
        
        if not self.current_championship.finished:
            self.show_ask_confirmation_screen()
            
        else:
            self.start_new_championship()
        
        return

    " *** Championships Screen - Functions *** "

    def start_new_championship(self):
        """
        Description:
            This function initializes all the variables for a new championship. 
            It also resets the images for the 8 teams shown in the new championship
            screen.

        Arguments:
            None.
    
        Returns:
            None.
    
        """
        
        # Reset variables
        
        self.last_team_ch_clicked = -1
        self.team_selected_players = [[], []]
        self.championship_teams = [[], [], [], [], [], [], [], []]
        
        # Reset images of teams
        
        for i in range(1,9):
            self.builder.get_object("b_player_1_team_%d" % i).set_from_file("images/teams/team_%d_p1.png" % i)
            self.builder.get_object("b_player_2_team_%d" % i).set_from_file("images/teams/team_%d_p2.png" % i)
        
        # Show New Championship screen
        
        self.show_new_championship_screen()
        
        return
    
    
    " *** New Championship Screen - Buttons Handlers *** "

    def b_create_championship_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when the create championship button
            is clicked. This button appears only when all the teams have been
            selected. It saves the championship in the database and takes
            the user to the championship matches screen.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
                
        # Save championship in DB
        
        db_handler.set_championship(self.championship_teams)
        
        self.current_championship = db_handler.get_championship()
        
        # Show championship screen
        
        self.show_championship_matches_screen()
        
        return
    
    def b_shuffle_matches_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when the shuffle matches button
            is clicked. The teams are left untouched while the positions are
            shuffled so that the matches are random. The teams are shuffled
            within the championship_teams list and then the player's images
            are reassigned to their new positions.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
        
        # Shuffle matches
                                
        random.shuffle(self.championship_teams)
                                
        # Reassign images
        
        for i in range(len(self.championship_teams)):
        
            p_1 = self.builder.get_object("b_player_1_team_%d" % (i+1))
            p_2 = self.builder.get_object("b_player_2_team_%d" % (i+1))
        
            if self.championship_teams[i] != []:
        
                small_ch_1 = GdkPixbuf.Pixbuf.new_from_file_at_size('images/characters/ch_%d.png' % self.championship_teams[i][0], 120, 148)
                small_ch_2 = GdkPixbuf.Pixbuf.new_from_file_at_size('images/characters/ch_%d.png' % self.championship_teams[i][1], 120, 148)
        
                p_1.set_from_pixbuf(small_ch_1)
                p_2.set_from_pixbuf(small_ch_2)
                
            else:
                
                p_1.set_from_file('images/teams/team_%d_p1.png' % (i+1))
                p_2.set_from_file('images/teams/team_%d_p2.png' % (i+1))
        
        return
    
    def b_shuffle_teams_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when the shuffle teams button is clicked.
            Here the team-heads are kept in their position while the team-tails 
            are shuffled between the possible teams.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
        
        # Shuffle teams
                                        
        team_tails = [team[1] for team in self.championship_teams if team != []]
                
        for i in range(len(self.championship_teams)):
            
            if self.championship_teams[i] == []: continue
            
            tail = random.choice(team_tails)
            
            self.championship_teams[i][1] = tail
                
            team_tails.pop(team_tails.index(tail))
                                            
        # Reassign images
        
        for i in range(len(self.championship_teams)):
        
            p_2 = self.builder.get_object("b_player_2_team_%d" % (i+1))
        
            if self.championship_teams[i] != []:
        
                small_ch_2 = GdkPixbuf.Pixbuf.new_from_file_at_size('images/characters/ch_%d.png' % self.championship_teams[i][1], 120, 148)
        
                p_2.set_from_pixbuf(small_ch_2)
                
            else:
                p_2.set_from_file('images/teams/team_%d_p2.png' % (i+1))
        
        return
    
    def b_team_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when one of the teams is clicked in the 
            new championship screen. All the teams are connected to the same 
            callback function and the number of team is gotten from the name stored
            the object. Then the select team screen is shown.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
        
        # Get the number of team selected
        
        self.last_team_selected = int(obj.get_name())
        
        # Show select team screen
        
        self.show_select_team_screen()
        
        return
    

    " *** Select Team Screen - Buttons Handlers *** "
    
    def b_team_ch_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when one of the characters is clicked 
            in the select team screen. The number of character clicked is gotten
            from the name stored in the object. The function also checks if the
            character was already assigned in another team. If it was already 
            assigned, the variable last_team_ch_clicked is set to -1 to represent
            that no character was selected yet.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
                        
        team_ch_n = int(obj.get_name())
        
        # Check if character is already in the championship
                
        for team in self.championship_teams:
            
            # Skip current selected team
            
            if team == self.championship_teams[self.last_team_selected - 1]: continue
            
            if self.ch_first_row[team_ch_n - 1] != 0 and self.ch_first_row[team_ch_n - 1] in team:
                self.last_team_ch_clicked = -1
                return
                                
        if len(self.ch_first_row) >= team_ch_n:
            self.last_team_ch_clicked = self.ch_first_row[team_ch_n - 1]
                  
        return

    def b_team_p1_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when player 1 is clicked in the select
            team screen from the new championship. If no character was selected
            previously, the function returns immediately. Otherwise, the character
            is placed in the player 1 spot. If the character was already in the 
            player 2 spot, it is moved to it's new spot. 

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
                
        if self.last_team_ch_clicked == -1: return
        
        if self.last_team_ch_clicked != 0:
            
            # Remove character from old player if already selected

            if [self.last_team_ch_clicked] == self.team_selected_players[1]:
                self.builder.get_object("team_p2").set_from_file("images/player_2.png")                
                self.team_selected_players[1] = []
                
        # Set the new player with this character
                    
        pl = self.builder.get_object("team_p1")
        
        pl.set_from_file("images/characters/ch_%d.png" % self.last_team_ch_clicked)
        
        self.team_selected_players[0] = [self.last_team_ch_clicked]
        
        self.update_team_rows_characters()
                
        return
    
    def b_team_p2_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when player 2 is clicked in the select
            team screen from the new championship. If no character was selected
            previously, the function returns immediately. Otherwise, the character
            is placed in the player 2 spot. If the character was already in the 
            player 1 spot, it is moved to it's new spot. 

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
                
        if self.last_team_ch_clicked == -1: return
        
        if self.last_team_ch_clicked != 0:
            
            # Remove character from old player if already selected

            if [self.last_team_ch_clicked] == self.team_selected_players[0]:
                self.builder.get_object("team_p1").set_from_file("images/player_1.png")                
                self.team_selected_players[0] = []
                
        # Set the new player with this character
                    
        pl = self.builder.get_object("team_p2")
        
        pl.set_from_file("images/characters/ch_%d.png" % self.last_team_ch_clicked)
        
        self.team_selected_players[1] = [self.last_team_ch_clicked]
        
        self.update_team_rows_characters()
                
        return
    
    def b_accept_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when the accept button is clicked in the
            team selection screen from the new championship. If there is an empty
            slot (player 1 or 2) the function returns immediately. Otherwise, the 
            team is saved in the championship_teams list. Also, the images of the
            players are reduced and placed accordingly in the championship teams
            screen. The team_selected_players list is emptied and the championship
            teams screen is shown.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
        
        if [] in self.team_selected_players: return
        
        # Save the team players in the championship list
        
        self.championship_teams[self.last_team_selected - 1] = [self.team_selected_players[0][0], self.team_selected_players[1][0]]
        
        # Set the selected team to the clicked team
        
        p_1 = self.builder.get_object("b_player_1_team_%d" % self.last_team_selected)
        p_2 = self.builder.get_object("b_player_2_team_%d" % self.last_team_selected)
        
        small_ch_1 = GdkPixbuf.Pixbuf.new_from_file_at_size('images/characters/ch_%d.png' % self.team_selected_players[0][0], 120, 148)
        small_ch_2 = GdkPixbuf.Pixbuf.new_from_file_at_size('images/characters/ch_%d.png' % self.team_selected_players[1][0], 120, 148)
        
        p_1.set_from_pixbuf(small_ch_1)
        p_2.set_from_pixbuf(small_ch_2)
        
        # Reset the selected players list
        
        self.team_selected_players = [[], []]
        
        # Show New Championship Screen
        
        self.show_new_championship_screen()
        
        return
    
    def b_up_2_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when the up arrow button is clicked in
            the select team screen from the new championship. It substracts 1 to
            the current_row variable if not in the first one and manages showing
            or hiding the buttons if in the first or last row accordingly.
            It also calls the function to update the row of characters.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
        
        # Substract 8 to elements of the ch_first_row list if not in the first row 
        if self.current_row != 1:
            self.current_row -= 1   
        
        # Hide up button if already in the first row
        if self.current_row == 1:
            self.builder.get_object("ebox_b_up_2").hide()
            
        # Show down button if not in the last row
        if self.current_row != self.last_row:
            self.builder.get_object("ebox_b_down_2").show()
        
        self.update_team_rows_characters()
        
        return
    
    def b_down_2_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when the up arrow button is clicked in
            the select team screen from the new championship. It adds 1 to
            the current_row variable if not in the last one and manages showing
            or hiding the buttons if in the first or last row accordingly.
            It also calls the function to update the row of characters.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
        # Add 8 to elements of the ch_team_row list if not in the last row
        if self.current_row != self.last_row:
            self.current_row += 1
        
        # Hide down button if already in the last row            
        if self.current_row == self.last_row:
            self.builder.get_object("ebox_b_down_2").hide()
            
        # Show up button if not in the first row
        if self.current_row != 1:
            self.builder.get_object("ebox_b_up_2").show()                          
                    
        self.update_team_rows_characters()
        
        return
    

    " *** Championship Matches Screen - Buttons Handlers *** "
    
    def b_match_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when a match is clicked in the championship
            matches screen. The number of match is gotten from the name of the object.
            Each match has two objects (two teams) that are assigned the same number,
            the number of match (from 1 to 14). If the match has already been played
            (there is a result other than -1, -1 stored) or both teams are not decided yet,
            the function returns immediately. The championship game is initialized and
            the championship match screen is shown.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
        
        # Set match number
        
        self.championship_match_n = int(obj.get_name())
        
        current_match = self.current_championship.matches[self.championship_match_n - 1]
        
        # If match already played or match not complete, do not respond
        
        if current_match[4:6] != [-1, -1] or -1 in current_match[0:4]: return
        
        # Initialize match variables
        
        self.initialize_championship_game()
        
        self.show_championship_match_screen()
        
        return
    
    def b_start_championship_match_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when the start match button is selected
            in the championship match screen. This screen appears after a match
            has been selected in the championship matches screen. This pre-match
            screen allows for the change in position (Defense - Attack) of the
            players of both teams. It calls the callback function of the start
            match button in the characters screen.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
        
        self.b_start_clicked(None)
        
        return
    

    " *** Seasons Screen - Buttons Handlers *** "
    
    def b_seasons_left_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when the left arrow is clicked in the
            seasons screen. The sid_shown is decreased if not in the first 
            season. It updates the season data shown and manages showing or hiding
            the left and right arrow buttons when on the first or last season.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
        
        if self.sid_shown > 1:
            
            self.sid_shown -= 1
            
            self.show_season_stats()
            
        if self.sid_shown == 1:
            self.builder.get_object("ebox_b_seasons_left").hide()
            
        if self.sid_shown < self.current_season_sid:
            self.builder.get_object("ebox_b_seasons_right").show()
        
        return
        
    def b_seasons_right_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when the right arrow is clicked in the
            seasons screen. The sid_shown is increased if not in the last
            season. It updates the season data shown and manages showing or hiding
            the left and right arrow buttons when on the first or last season.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
        
        if self.sid_shown < self.current_season_sid:
            
            self.sid_shown += 1
            
            self.show_season_stats()
            
        if self.sid_shown == self.current_season_sid:
            self.builder.get_object("ebox_b_seasons_right").hide()
            
        if self.sid_shown > 1:
            self.builder.get_object("ebox_b_seasons_left").show()
        
        return      
    
    
    " *** Seasons Screen - Functions *** "
    
    def show_season_stats(self):
        """
        Description:
            This function gets the data to be displayed in the seasons screen.
            It converts the season number to roman and sets the string to the
            title. It gets the data of the 7-0 matches from the database. The
            data is shown inside the 32 slots available.
            Also, the players with the most number of 7-0 and 0-7 are shown as
            losser and winner of the season. Officially, there is no losser of 
            the season and the winner is the player who has suffered from most
            7-0 against them.

        Arguments:
            None.
    
        Returns:
            None.
    
        """
        
        # Check if self.sid_shown if from current season (not stored in DB yet)
        
        if self.sid_shown > db_handler.get_last_sid():
            
            # Get season number
            
            season_n = db_handler.get_last_season_n() + 1
            
        else:
            
            # Get season number
            
            season_n = db_handler.get_season_n(self.sid_shown)
        
        # Convert season_n to roman numeral (Handles only up to 100)
        
        season_n_roman = self.num_to_roman(season_n) 
            
        self.builder.get_object("seasons_headline").set_text("GLORIOSA TEMPORADA %5s DE FUTBOLIN" % season_n_roman)
            
        # Get Match data from the database
                
        matches_list = db_handler.get_7_0_matches(self.sid_shown)
        
        # Count each players 7 - 0 and 0 - 7 count

        matches_won = []
        matches_lost = []
        
        for i in range(len(matches_list)):
            
            pid_1 = matches_list[i].mad_def_pid
            pid_2 = matches_list[i].mad_atk_pid
            pid_3 = matches_list[i].atl_def_pid
            pid_4 = matches_list[i].atl_atk_pid
                       
            s_mad = matches_list[i].score_mad
            s_atl = matches_list[i].score_atl
            
            if (s_mad == 7 and s_atl == 0):
                pid_1, pid_3 = pid_3, pid_1
                pid_2, pid_4 = pid_4, pid_2
             
            # Increment count in matches_lost for pid_1 and pid_2
            
            matches_lost.append(pid_1)
            matches_lost.append(pid_2)
             
            # Increment count in matches_won for 
            
            matches_won.append(pid_3)
            matches_won.append(pid_4)
            
            # Get players nicknames
            
            p_1 = db_handler.get_player_nickname(pid_1)
            p_2 = db_handler.get_player_nickname(pid_2)
            p_3 = db_handler.get_player_nickname(pid_3)
            p_4 = db_handler.get_player_nickname(pid_4)
            
            c_1 = matches_lost.count(pid_1)
            c_2 = matches_lost.count(pid_2)
            c_3 = matches_won.count(pid_3)
            c_4 = matches_won.count(pid_4)
            
            self.builder.get_object("stats_line_%d" % (i+1)).set_text("%02x (%2d)%15s - %-15s(%2d) (%2d)%15s - %-15s(%2d)" % (i, c_1, p_1, p_2, c_2, c_3, p_3, p_4, c_4))
            self.builder.get_object("stats_line_%d" % (i+1)).show()
        
        for i in range(len(matches_list), 32):
            self.builder.get_object("stats_line_%d" % (i+1)).hide()
            
        # Show season winner(s)
        
        won_match_count = matches_won.count(max(set(matches_won), key=matches_won.count))
        
        winners_pids = set([pid for pid in matches_won if matches_won.count(pid) == won_match_count])
                
        winners_nicknames = [db_handler.get_player_nickname(p) for p in winners_pids]
        
        self.builder.get_object("seasons_winner").set_text(", ".join(winners_nicknames)[:23]) # Crop text to 23 spaces max
                
        # Show season losers
        
        lost_match_count = matches_lost.count(max(set(matches_lost), key=matches_lost.count))
        
        lossers_pids = set([pid for pid in matches_lost if matches_lost.count(pid) == lost_match_count])
                
        lossers_nicknames = [db_handler.get_player_nickname(p) for p in lossers_pids]
        
        self.builder.get_object("seasons_losser").set_text(", ".join(lossers_nicknames)[:23]) # Crop text to 23 spaces max
                
        return
    
    def num_to_roman(self, n):
        """
        Description:
            This function converts an integer number into its roman representation

        Arguments:
            n (Integer): Number to convert to roman.
    
        Returns:
            None.
    
        """
        
        units=("", "I", "II", "III",  "IV", "V",
               "VI", "VII", "VIII", "IX")
        tens=("", "X", "XX", "XXX", "XL", "L",
              "LX", "LXX", "LXXX", "XC")
        
        ten, unit = divmod(n, 10)
                        
        return tens[ten]+units[unit]
    
    
    " *** Stats Screen - Buttons Handlers *** "
    
    def b_stats_left_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when the left arrow button is clicked
            in the stats screen. It decrements the stats_ch_i variable if not
            in the first character. It manages the hiding or showing of the arrows
            depending if on the first or last character. It sets the new image 
            for the character and then updates the text in the stats.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
        
        if self.stats_ch_i > 1:
            self.stats_ch_i -= 1
            
        if self.stats_ch_i == 1:
            self.builder.get_object("ebox_b_stats_left").hide()
        
        if self.stats_ch_i < (len(self.ch_order) - 1):
            self.builder.get_object("ebox_b_stats_right").show()
        
        # Set the new player image
        
        ch = self.builder.get_object("stats_ch")  
            
        big_ch = GdkPixbuf.Pixbuf.new_from_file_at_size('images/characters/ch_%d.png' % self.ch_order[self.stats_ch_i], 210, 240)
        
        ch.set_from_pixbuf(big_ch)
        
        # Update the new player's stats
                
        self.update_ch_stats()
        
        return
    
    def b_stats_right_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when the right arrow button is clicked
            in the stats screen. It increments the stats_ch_i variable if not
            in the last character. It manages the hiding or showing of the arrows
            depending if on the first or last character. It sets the new image 
            for the character and then updates the text in the stats.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
        
        if self.stats_ch_i < (len(self.ch_order) - 1):
            self.stats_ch_i += 1
            
        if self.stats_ch_i == (len(self.ch_order) - 1):
            self.builder.get_object("ebox_b_stats_right").hide()
        
        if self.stats_ch_i > 1:
            self.builder.get_object("ebox_b_stats_left").show()
        
        ch = self.builder.get_object("stats_ch")  
            
        big_ch = GdkPixbuf.Pixbuf.new_from_file_at_size('images/characters/ch_%d.png' % self.ch_order[self.stats_ch_i], 210, 240)
        
        ch.set_from_pixbuf(big_ch)
        
        # Update the new player's stats
        
        self.update_ch_stats()
        
        return
    
    
    " *** Stats Screen - Functions *** "
    
    def update_ch_stats(self):
        """
        Description:
            This function updates the nickname and the stats data in the stats screen
            for the current character being shown. The character shown is determined
            by the variable stats_ch_i.

        Arguments:
            None.
    
        Returns:
            None.
    
        """
        
        # Update the nickname
        
        self.builder.get_object("stats_nickname").set_text(db_handler.get_player_nickname(self.ch_order[self.stats_ch_i]))
        
        # Get stats for character
        
        ch = db_handler.get_player(self.ch_order[self.stats_ch_i])
        
        ch_data = ["%s min" % (ch.play_time/60),
                   "%s/%s" % (ch.mad_wins + ch.atl_wins, ch.n_matches),
                   "%s" % ch.n_goals,
                   "%s" % ch.n_autogoals,
                   "%s" % ch.n_rcvd_goals]
        
        # Update the text with the character's data
        
        for i in range(5):
            old_text = self.builder.get_object("stats_data_%d" % (i+1)).get_text()
            
            new_text = "%-17s: %11s" % (old_text.split(':')[0] ,ch_data[i])
            
            self.builder.get_object("stats_data_%d" % (i+1)).set_text(new_text)
    
        return
    
    
    " *** Match Screen - Buttons Handlers *** "
    
    def b_score_mad_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when the Madrid score number is clicked.
            The action is to erase the last goal of that team and update the 
            screen and the match status. If the match was over, and after 
            subtracting the goal it begins again, a thread is launched to start
            counting time again.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
        
        if (self.score_mad == 0):
            return
        
        # Subtract last goal from Madrid                
            
        self.score_mad -= 1
        
        self.goals_mad.pop()
        
        # Update scoreboard
        
        self.update_scoreboard()
        
        # Update goals dash
                    
        self.update_goal_dash()
        
        # Update remaining balls
        
        self.update_remaining_balls()
        
        # Update match status
        
        if not (self.in_match):
            self.update_match_status()
            
            if self.in_match:
                self.start_thread(self.count_time)                            
                self.builder.get_object("ebox_b_end_match").hide()
        
        return
    
    def b_score_atl_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when the Atletico score number is clicked.
            The action is to erase the last goal of that team and update the 
            screen and the match status. If the match was over, and after 
            subtracting the goal it begins again, a thread is launched to start
            counting time again.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
        
        if (self.score_atl == 0):
            return
        
        # Subtract last goal from Atletico 
                            
        self.score_atl -= 1
        
        self.goals_atl.pop()
        
        # Update scoreboard
        
        self.update_scoreboard()
        
        # Update goals dash 
        
        self.update_goal_dash()
                    
        # Update remaining balls
        
        self.update_remaining_balls()
        
        # Update match status 
        
        if not (self.in_match):
            self.update_match_status()
            
            if self.in_match:
                self.start_thread(self.count_time)                            
                self.builder.get_object("ebox_b_end_match").hide()
        
        return
       
    def b_end_match_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when the end match button is clicked after
            a match is finished. The button appears only when the score determines
            the match is done. If the score was 7-0, music is played.
            The match is saved and a thread is launched to post the result on a tweet
            if none of the characters is anonymous.
            If in a championship, the match is saved into the championship. If the 
            championship finishes, it shows the congratulations screen. If not, it shows
            the championship matches screen.
            If in mundialito mode, the next match is loaded and started.
            If in normal mode, the user is taken to the characters screen, while
            keeping the winner team selected for the next match.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
            
        # Check if it was 7-0 match
        
        if (self.score_mad == 7 or self.score_atl == 7):
            self.play_sound("7_0.wav")
        else:
            self.play_sound("select_1.wav")
            
        # Save match data
        
        if not self.saving:
            self.start_thread(self.save_match_data, ())
        
        # Post tweet with the result of the match
        
        if ('dev' not in sys.argv) and ([0] not in self.selected_players):
            self.start_thread(self.post_result_tweet)
        
        # If it is a championship match, return to championship screen
        
        if self.championship_match:
            
            self.save_championship_data()
                
            # If championship is finished, show congratulations screen
            
            if self.current_championship.finished:
                
                self.builder.get_object("winner_1").set_from_file("images/characters/ch_%d.png" % self.current_championship.winner[0])
                self.builder.get_object("winner_2").set_from_file("images/characters/ch_%d.png" % self.current_championship.winner[1])
                
                self.builder.get_object("winner_1_txt").set_text(db_handler.get_player_nickname(self.current_championship.winner[0]))
                self.builder.get_object("winner_2_txt").set_text(db_handler.get_player_nickname(self.current_championship.winner[1]))
                
                self.show_congratulations_screen()
    
            # Else, return to championship matches screen
            
            else:
                self.show_championship_matches_screen()
            
            return
        
        # If in mundialito mode, start the next match
        
        if self.mundialito_mode and len(self.mundialito_matches) > 0:
            
            # Delete last mundialito match
            
            self.mundialito_matches.pop(0)
            
            # Check if there are any matches left
            
            if len(self.mundialito_matches) == 0:
                self.mundialito_mode = False
                self.show_characters_screen()
                self.initialize_game()
                return
            
            # Change match formation to the next one
            
            self.selected_players = self.mundialito_matches[0][:]
            
            # Update players images
            
            for i in range(4):
                self.builder.get_object("player_%d" % (i+1)).set_from_file("images/characters/ch_%d.png" % self.selected_players[i][0])                                    
            
            # Update mundialito message
            
            self.builder.get_object("mundialito_msg").set_text("Mundialito %d/4" % (5 - len(self.mundialito_matches)))
            
            self.b_start_clicked(obj, data)
            
            return
        
        # Show characters screen
                        
        self.show_characters_screen()
        
        # Initialize new game
        
        self.initialize_game(keep="winner")
        
        return
    
    def b_cancel_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when the cancel button is clicked during
            a match. The user is prompted with the ask confirmation screen.
            The match is paused during this.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
        
        self.match_b_clicked = "cancel"
        
        self.in_match = False
        
        self.show_ask_confirmation_screen()
        
        return
        
    def b_reset_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when the reset button is clicked during 
            a match. If the match is already finished, the function returns
            immediately. The user is prompted with the ask confirmation screen.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
        
        if not self.in_match: return
         
        self.match_b_clicked = "reset"
        
        self.in_match = False
        
        self.show_ask_confirmation_screen()
        
        return
    
    
    " *** Match Screen - Functions *** "
    
    def mad_goal(self, ask_scorer=True):
        """
        Description:
            This function plays a random goal sound, saves the time of
            the goal and the team that was responsible for the goal. 
            If not coming from the remote control (ask_scorer=True) the 
            user is prompted with the select scorer screen with a timeout
            value that is also set here.

        Arguments:
            ask_scorer (Boolean): It determines whether to prompt the user
            with the select scorer screen or not. If the call is done from
            the remote control, it does not need to ask scorer.
    
        Returns:
            None.
    
        """
        
        # Play goal sound
        
        self.play_sound(random.choice(["goal_5.wav", "goal_6.wav", "goal_7.wav", "goal_8.wav"]))
        
        self.last_goal_team = "mad"                
        self.last_goal_time = self.match_time
        
        if ask_scorer:
            self.ask_timeout = 8
                    
            self.show_ask_scorer_screen()
                                                                                              
        return
    
    def atl_goal(self, ask_scorer=True):
        """
        Description:
            This function plays a random goal sound, saves the time of
            the goal and the team that was responsible for the goal. 
            If not coming from the remote control (ask_scorer=True) the 
            user is prompted with the select scorer screen with a timeout
            value that is also set here.

        Arguments:
            ask_scorer (Boolean): It determines whether to prompt the user
            with the select scorer screen or not. If the call is done from
            the remote control, it does not need to ask scorer.
    
        Returns:
            None.
    
        """
        
        # Play goal sound
        
        self.play_sound(random.choice(["goal_5.wav", "goal_6.wav", "goal_7.wav", "goal_8.wav"]))
                                                
        self.last_goal_team = "atl"
        self.last_goal_time = self.match_time
        
        if ask_scorer:
            self.ask_timeout = 8
                            
            self.show_ask_scorer_screen()
                                                     
        return
    
    def show_match_stats(self):
        """
        Description:
            This function handles what statistics to show during a match.
            If anonymous is playing, no stats are shown. Otherwise, when
            the match starts, a bargraph is shown with the historic results
            of the teams playing. After each goal, a positive stat of the 
            scoring team or a negative stat of the opposite team is shown.

        Arguments:
            None.
    
        Returns:
            None.
    
        """
        
        stats_single_good = [["Goles Legales", db_handler.get_season_legal_goals],
                             ["Partidos Ganados", db_handler.get_season_matches_won]]
        
        stats_single_bad = [["Goles Recibidos", db_handler.get_season_goals_against],
                            ["Partidos Perdidos", db_handler.get_season_matches_lost]]

        stats_team_good = [["Partidos Ganados", db_handler.get_team_matches_won]]
        
        stats_team_bad = [["Partidos Perdidos", db_handler.get_team_matches_lost]]
                
        stats_illegal = [["Goles Ilegales", db_handler.get_season_illegal_goals]]
        
                
        # If all players are anonymous or one is anonymous and score is 0 - 0, hide all and return
                
        if (self.selected_players == [[0], [0], [0], [0]]) or ((self.score_mad + self.score_atl == 0) and [0] in self.selected_players):
                        
            self.builder.get_object("bargraph_base").hide()
            for i in range(12):
                self.builder.get_object("bargraph_%d" % (i+1)).hide()
                
            self.builder.get_object("stats_match_description").hide()
            self.builder.get_object("stats_match_nickname").hide()
            self.builder.get_object("stats_match_data").hide()            
                        
            return
        
        # If score is 0 - 0 and anonymous is not playing, show the bargraph with the results stats
        
        if (self.score_mad + self.score_atl == 0) and ([0] not in self.selected_players):
            
            self.builder.get_object("stats_match_description").hide()
            self.builder.get_object("stats_match_nickname").hide()
            self.builder.get_object("stats_match_data").hide()
             
            self.get_match_stats()
             
        # If score is different from 0 - 0
        
        elif (self.score_mad + self.score_atl != 0):
            
            self.builder.get_object("bargraph_base").hide()
            for i in range(12):
                self.builder.get_object("bargraph_%d" % (i+1)).hide()    
        
            # If anonymous is not playing
            
            if [0] not in self.selected_players:                                        
            
                # *** Choose stats function according to last goal *** 
                
                # 1) If goal is illegal, show bad stats about scorer
                
                if not self.legal or (self.last_goal_team == "mad" and 12 <= self.last_scorer <= 22) or (self.last_goal_team == "atl" and 1 <= self.last_scorer <= 11):
                    
                    # Bad stats about scorer
                        
                    stats_description, stats_function = random.choice(stats_illegal)
                    
                    player_i = self.get_player_index(self.last_scorer)
                    
                    player_pid = self.selected_players[player_i][0]
            
                    player_nickname = self.selected_players_names[player_i]
                    
                    stats_data = stats_function(player_pid)
                
                
                # 2) If goal is legal...
                
                else:
                                        
                    # Single Player Statistics 
                    
                    if random.randint(0,1):
                         
                        # Single Player - Good Statistics (scorer)
                        
                        if random.randint(0,1):
                        
                            stats_description, stats_function = random.choice(stats_single_good)
                            player_i = self.get_player_index(self.last_scorer)
                            
                        # Single Player - Bad Statistics (goal keeper or other)
                            
                        else:
                            
                            stats_description, stats_function = random.choice(stats_single_bad)
                            player_i = random.randint(0,1)
                            
                            if self.last_goal_team == "mad":
                                player_i += 2
                                
                        # Get player data
                        
                        player_pid = self.selected_players[player_i][0]
            
                        player_nickname = self.selected_players_names[player_i]
                                
                        # Call Single Player stats function
                        
                        stats_data = stats_function(player_pid)

                    # Team Statistics

                    else:
                        
                        # Team - Good Statistics
                        
                        if random.randint(0,1):
                                                                                
                            stats_description, stats_function = random.choice(stats_team_good)
                            
                            if self.last_goal_team == "mad":
                                team_pids = [p[0] for p in self.selected_players[0:2]]
                                player_nickname = '\n'.join(self.selected_players_names[0:2])
                            else:
                                team_pids = [p[0] for p in self.selected_players[2:4]]
                                player_nickname = '\n'.join(self.selected_players_names[2:4])
                        
                        # Team - Bad Statistics
                        
                        else:                                                
                        
                            stats_description, stats_function = random.choice(stats_team_bad)
                            
                            if self.last_goal_team == "mad":
                                team_pids = [p[0] for p in self.selected_players[2:4]]
                                player_nickname = '\n'.join(self.selected_players_names[2:4])
                            else:
                                team_pids = [p[0] for p in self.selected_players[0:2]]
                                player_nickname = '\n'.join(self.selected_players_names[0:2])
                            
                        # Call Team stats function
                        
                        stats_data = stats_function(*team_pids)
            
                # Write DB data into variable
                
                if 'stats_data' in locals():
                    stats_data_str = '/'.join([str(data) for data in stats_data])
                else:
                    stats_data_str = '-'
            
            # Anonymous is playing, show nothing (TODO: Show stats from the players that are not anonymous)
            
            else:
                
                stats_description = ''
                player_nickname = ''
                stats_data_str = ''
                
            # Write data in widgets
                
            self.builder.get_object("stats_match_description").set_text(stats_description)
            self.builder.get_object("stats_match_nickname").set_text(player_nickname)
            self.builder.get_object("stats_match_data").set_text(stats_data_str)    
            
        return
    
    def save_match_data(self):
        """
        Description:
            This function saves the match and goals data in the database.
            If anonymous is playing, it returns immediately without saving.
            It also updates the data in the Players table of the DB.
            Finally, it checks if the season is over to start a new one.

        Arguments:
            None.
    
        Returns:
            None.
    
        """
        
        self.saving = True
        
        # If anonymous is playing, do not save match data
        
        if [0] in self.selected_players:
            return
                                        
        # Save match data in Matches table
        
        mad_def_pid = self.selected_players[0][0]
        mad_atk_pid = self.selected_players[1][0]
        atl_def_pid = self.selected_players[2][0]
        atl_atk_pid = self.selected_players[3][0]
        
        date = time.strftime("%Y-%m-%d")
        
        mid = db_handler.set_match(mad_def_pid, mad_atk_pid, atl_def_pid, atl_atk_pid, self.score_mad, self.score_atl, date, self.start_time, self.match_time)
        
        # Save goals data of Madrid in Goals table
        
        for goal in self.goals_mad:
            pid         = goal[0]
            goal_time   = goal[1]
            figure      = (goal[2] - 1) % 11 + 1
            
            if 1 <= goal[2] <= 11:
                legal   = goal[3]
            else:
                legal   = False
                        
            db_handler.set_goal(mid, pid, goal_time, figure, legal)
            
        # Save goals data of Atletico in Goals table
        
        for goal in self.goals_atl:
            pid         = goal[0]
            goal_time   = goal[1]
            figure      = (goal[2] - 1) % 11 + 1
            
            if 12 <= goal[2] <= 22:
                legal   = goal[3]
            else:
                legal   = False
                        
            db_handler.set_goal(mid, pid, goal_time, figure, legal)
            
        # Save data in Players table
        
        db_handler.update_players_table_data([mad_def_pid, mad_atk_pid, atl_def_pid, atl_atk_pid])
                        
        # Check if season is over
        
        current_season_7_0 = db_handler.get_7_0_matches(db_handler.get_last_sid() + 1)
                
        if len(current_season_7_0) == 32:
            
            self.logger.info("Saving season...")
            
            # Get current season number
            
            season_n = db_handler.get_last_season_n() + 1
                        
            # Get current season initial mid
            
            initial_mid = db_handler.get_season().final_mid + 1
                        
            # Get current season final mid
            
            final_mid = db_handler.get_last_mid()
                        
            # Save season
            
            db_handler.set_season(season_n, initial_mid, final_mid)
            
            self.logger.info("Season saved.")
                
        return
    
    def save_championship_data(self):
        """
        Description:
            This function updates the championship data in the database after a 
            championship match. If it was the last match, it stores also the winner,
            runner-up and sets the finnished flag to True. Otherwise, it sets
            the winning team for the following match.

        Arguments:
            None.
    
        Returns:
            None.
    
        """
                
        # Update match scores
        
        self.current_championship.matches[self.championship_match_n - 1][4] = self.score_mad
        self.current_championship.matches[self.championship_match_n - 1][5] = self.score_atl
        
        # Update winner for next match if any
        
        if self.championship_match_n != 7:
            
            next_match_i = (self.championship_match_n - 1) / 2 + 4
            next_match_team = (self.championship_match_n - 1) % 2 * 2
                        
            if self.score_mad > self.score_atl:           
                self.current_championship.matches[next_match_i][next_match_team:next_match_team + 2] = sorted(self.selected_players[0] + self.selected_players[1])
                
            else:
                self.current_championship.matches[next_match_i][next_match_team:next_match_team + 2] = sorted(self.selected_players[2] + self.selected_players[3])
        
        # Update winner, runner-up and finished flag in match_n == 7
        
        else:
            
            if self.score_mad > self.score_atl:
                self.current_championship.winner    = sorted(self.selected_players[0] + self.selected_players[1])
                self.current_championship.runner_up = sorted(self.selected_players[2] + self.selected_players[3])
                
            else:
                self.current_championship.winner    = sorted(self.selected_players[2] + self.selected_players[3])
                self.current_championship.runner_up = sorted(self.selected_players[0] + self.selected_players[1])
                
            self.current_championship.finished = True
        
        # Update championship data in DB
            
        db_handler.update_championship(self.current_championship)
        
        return
    
    def post_result_tweet(self):
        """
        Description:
            This function is in charge of generating the string that is
            going to be tweeted after each match that is finnished.
            The tweet post is handled by the twitter_handler module.

        Arguments:
            None.
    
        Returns:
            None.
    
        """
        
        tweet_text = "%s - %s (%d - %d) %s - %s" % \
                     (self.selected_players_names[0],
                      self.selected_players_names[1],
                      self.score_mad,
                      self.score_atl,
                      self.selected_players_names[2],
                      self.selected_players_names[3])
        
        twitter_handler.post_tweet(tweet_text)
        
        return
        
                                                
    " *** Ask Scorer Screen - Buttons Handlers *** "

    # Each scorer has its own function to speed up GUI response
    # The other approach is giving an id (name) to each ebox but obj.get_name() is too slow
    # Try calling scorer_clicked in a thread to make it even faster

    def b_mad_1_clicked(self, obj, data=None, use_threads=True):
        """
        Description:
            This function takes action when this particular scorer is clicked
            in the select scorer screen. The variable last_scorer is set, the
            legal flag is set correspondingly and a function is called to 
            handle the score.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
            use_threads (boolean): This argument defines whether to use or not
            threads inside the function. Threads are not used
            when the call comes from the web service (remote control).
    
        Returns:
            None.
    
        """
        
        self.last_scorer = 1
        self.legal = True
        self.scorer_clicked(use_threads) 

    def b_mad_2_clicked(self, obj, data=None, use_threads=True):
        """
        Description:
            This function takes action when this particular scorer is clicked
            in the select scorer screen. The variable last_scorer is set, the
            legal flag is set correspondingly and a function is called to 
            handle the score.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
            use_threads (boolean): This argument defines whether to use or not
            threads inside the function. Threads are not used
            when the call comes from the web service (remote control).
    
        Returns:
            None.
    
        """
        
        self.last_scorer = 2
        self.legal = True
        self.scorer_clicked(use_threads)
        
    def b_mad_3_clicked(self, obj, data=None, use_threads=True):
        """
        Description:
            This function takes action when this particular scorer is clicked
            in the select scorer screen. The variable last_scorer is set, the
            legal flag is set correspondingly and a function is called to 
            handle the score.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
            use_threads (boolean): This argument defines whether to use or not
            threads inside the function. Threads are not used
            when the call comes from the web service (remote control).
    
        Returns:
            None.
    
        """
        
        self.last_scorer = 3
        self.legal = True
        self.scorer_clicked(use_threads)

    def b_mad_4_clicked(self, obj, data=None, use_threads=True):
        """
        Description:
            This function takes action when this particular scorer is clicked
            in the select scorer screen. The variable last_scorer is set, the
            legal flag is set correspondingly and a function is called to 
            handle the score.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
            use_threads (boolean): This argument defines whether to use or not
            threads inside the function. Threads are not used
            when the call comes from the web service (remote control).
    
        Returns:
            None.
    
        """
        
        self.last_scorer = 4
        if self.last_goal_team == "mad":
            self.ask_legal(use_threads)
        elif self.last_goal_team == "atl":
            self.legal = True
            self.scorer_clicked(use_threads)
                    
    def b_mad_5_clicked(self, obj, data=None, use_threads=True):
        """
        Description:
            This function takes action when this particular scorer is clicked
            in the select scorer screen. The variable last_scorer is set, the
            legal flag is set correspondingly and a function is called to 
            handle the score.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
            use_threads (boolean): This argument defines whether to use or not
            threads inside the function. Threads are not used
            when the call comes from the web service (remote control).
    
        Returns:
            None.
    
        """
        
        self.last_scorer = 5
        if self.last_goal_team == "mad":
            self.ask_legal(use_threads)
        elif self.last_goal_team == "atl":
            self.legal = True
            self.scorer_clicked(use_threads)

    def b_mad_6_clicked(self, obj, data=None, use_threads=True):
        """
        Description:
            This function takes action when this particular scorer is clicked
            in the select scorer screen. The variable last_scorer is set, the
            legal flag is set correspondingly and a function is called to 
            handle the score.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
            use_threads (boolean): This argument defines whether to use or not
            threads inside the function. Threads are not used
            when the call comes from the web service (remote control).
    
        Returns:
            None.
    
        """
        
        self.last_scorer = 6
        if self.last_goal_team == "mad":
            self.ask_legal(use_threads)
        elif self.last_goal_team == "atl":
            self.legal = True
            self.scorer_clicked(use_threads)
        
    def b_mad_7_clicked(self, obj, data=None, use_threads=True):
        """
        Description:
            This function takes action when this particular scorer is clicked
            in the select scorer screen. The variable last_scorer is set, the
            legal flag is set correspondingly and a function is called to 
            handle the score.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
            use_threads (boolean): This argument defines whether to use or not
            threads inside the function. Threads are not used
            when the call comes from the web service (remote control).
    
        Returns:
            None.
    
        """
        
        self.last_scorer = 7
        if self.last_goal_team == "mad":
            self.ask_legal(use_threads)
        elif self.last_goal_team == "atl":
            self.legal = True
            self.scorer_clicked(use_threads)

    def b_mad_8_clicked(self, obj, data=None, use_threads=True):
        """
        Description:
            This function takes action when this particular scorer is clicked
            in the select scorer screen. The variable last_scorer is set, the
            legal flag is set correspondingly and a function is called to 
            handle the score.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
            use_threads (boolean): This argument defines whether to use or not
            threads inside the function. Threads are not used
            when the call comes from the web service (remote control).
    
        Returns:
            None.
    
        """
        
        self.last_scorer = 8
        if self.last_goal_team == "mad":
            self.ask_legal(use_threads)
        elif self.last_goal_team == "atl":
            self.legal = True
            self.scorer_clicked(use_threads)
        
    def b_mad_9_clicked(self, obj, data=None, use_threads=True):
        """
        Description:
            This function takes action when this particular scorer is clicked
            in the select scorer screen. The variable last_scorer is set, the
            legal flag is set correspondingly and a function is called to 
            handle the score.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
            use_threads (boolean): This argument defines whether to use or not
            threads inside the function. Threads are not used
            when the call comes from the web service (remote control).
    
        Returns:
            None.
    
        """
        
        self.last_scorer = 9
        if self.last_goal_team == "mad":
            self.ask_legal(use_threads)
        elif self.last_goal_team == "atl":
            self.legal = True
            self.scorer_clicked(use_threads)

    def b_mad_10_clicked(self, obj, data=None, use_threads=True):
        """
        Description:
            This function takes action when this particular scorer is clicked
            in the select scorer screen. The variable last_scorer is set, the
            legal flag is set correspondingly and a function is called to 
            handle the score.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
            use_threads (boolean): This argument defines whether to use or not
            threads inside the function. Threads are not used
            when the call comes from the web service (remote control).
    
        Returns:
            None.
    
        """
        
        self.last_scorer = 10
        self.legal = True
        self.scorer_clicked(use_threads)
        
    def b_mad_11_clicked(self, obj, data=None, use_threads=True):
        """
        Description:
            This function takes action when this particular scorer is clicked
            in the select scorer screen. The variable last_scorer is set, the
            legal flag is set correspondingly and a function is called to 
            handle the score.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
            use_threads (boolean): This argument defines whether to use or not
            threads inside the function. Threads are not used
            when the call comes from the web service (remote control).
    
        Returns:
            None.
    
        """
        
        self.last_scorer = 11
        self.legal = True
        self.scorer_clicked(use_threads)
        
    def b_atl_1_clicked(self, obj, data=None, use_threads=True):
        """
        Description:
            This function takes action when this particular scorer is clicked
            in the select scorer screen. The variable last_scorer is set, the
            legal flag is set correspondingly and a function is called to 
            handle the score.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
            use_threads (boolean): This argument defines whether to use or not
            threads inside the function. Threads are not used
            when the call comes from the web service (remote control).
    
        Returns:
            None.
    
        """
        
        self.last_scorer = 12
        self.legal = True
        self.scorer_clicked(use_threads)

    def b_atl_2_clicked(self, obj, data=None, use_threads=True):
        """
        Description:
            This function takes action when this particular scorer is clicked
            in the select scorer screen. The variable last_scorer is set, the
            legal flag is set correspondingly and a function is called to 
            handle the score.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
            use_threads (boolean): This argument defines whether to use or not
            threads inside the function. Threads are not used
            when the call comes from the web service (remote control).
    
        Returns:
            None.
    
        """
        
        self.last_scorer = 13
        self.legal = True
        self.scorer_clicked(use_threads)
        
    def b_atl_3_clicked(self, obj, data=None, use_threads=True):
        """
        Description:
            This function takes action when this particular scorer is clicked
            in the select scorer screen. The variable last_scorer is set, the
            legal flag is set correspondingly and a function is called to 
            handle the score.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
            use_threads (boolean): This argument defines whether to use or not
            threads inside the function. Threads are not used
            when the call comes from the web service (remote control).
    
        Returns:
            None.
    
        """
        
        self.last_scorer = 14
        self.legal = True
        self.scorer_clicked(use_threads)

    def b_atl_4_clicked(self, obj, data=None, use_threads=True):
        """
        Description:
            This function takes action when this particular scorer is clicked
            in the select scorer screen. The variable last_scorer is set, the
            legal flag is set correspondingly and a function is called to 
            handle the score.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
            use_threads (boolean): This argument defines whether to use or not
            threads inside the function. Threads are not used
            when the call comes from the web service (remote control).
    
        Returns:
            None.
    
        """
        
        self.last_scorer = 15
        if self.last_goal_team == "atl":
            self.ask_legal(use_threads)
        elif self.last_goal_team == "mad":
            self.legal = True
            self.scorer_clicked(use_threads)
        
    def b_atl_5_clicked(self, obj, data=None, use_threads=True):
        """
        Description:
            This function takes action when this particular scorer is clicked
            in the select scorer screen. The variable last_scorer is set, the
            legal flag is set correspondingly and a function is called to 
            handle the score.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
            use_threads (boolean): This argument defines whether to use or not
            threads inside the function. Threads are not used
            when the call comes from the web service (remote control).
    
        Returns:
            None.
    
        """
        
        self.last_scorer = 16
        if self.last_goal_team == "atl":
            self.ask_legal(use_threads)
        elif self.last_goal_team == "mad":
            self.legal = True
            self.scorer_clicked(use_threads)

    def b_atl_6_clicked(self, obj, data=None, use_threads=True):
        """
        Description:
            This function takes action when this particular scorer is clicked
            in the select scorer screen. The variable last_scorer is set, the
            legal flag is set correspondingly and a function is called to 
            handle the score.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
            use_threads (boolean): This argument defines whether to use or not
            threads inside the function. Threads are not used
            when the call comes from the web service (remote control).
    
        Returns:
            None.
    
        """
        
        self.last_scorer = 17
        if self.last_goal_team == "atl":
            self.ask_legal(use_threads)
        elif self.last_goal_team == "mad":
            self.legal = True
            self.scorer_clicked(use_threads)
        
    def b_atl_7_clicked(self, obj, data=None, use_threads=True):
        """
        Description:
            This function takes action when this particular scorer is clicked
            in the select scorer screen. The variable last_scorer is set, the
            legal flag is set correspondingly and a function is called to 
            handle the score.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
            use_threads (boolean): This argument defines whether to use or not
            threads inside the function. Threads are not used
            when the call comes from the web service (remote control).
    
        Returns:
            None.
    
        """
        
        self.last_scorer = 18
        if self.last_goal_team == "atl":
            self.ask_legal(use_threads)
        elif self.last_goal_team == "mad":
            self.legal = True
            self.scorer_clicked(use_threads)

    def b_atl_8_clicked(self, obj, data=None, use_threads=True):
        """
        Description:
            This function takes action when this particular scorer is clicked
            in the select scorer screen. The variable last_scorer is set, the
            legal flag is set correspondingly and a function is called to 
            handle the score.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
            use_threads (boolean): This argument defines whether to use or not
            threads inside the function. Threads are not used
            when the call comes from the web service (remote control).
    
        Returns:
            None.
    
        """
        
        self.last_scorer = 19
        if self.last_goal_team == "atl":
            self.ask_legal(use_threads)
        elif self.last_goal_team == "mad":
            self.legal = True
            self.scorer_clicked(use_threads)
        
    def b_atl_9_clicked(self, obj, data=None, use_threads=True):
        """
        Description:
            This function takes action when this particular scorer is clicked
            in the select scorer screen. The variable last_scorer is set, the
            legal flag is set correspondingly and a function is called to 
            handle the score.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
            use_threads (boolean): This argument defines whether to use or not
            threads inside the function. Threads are not used
            when the call comes from the web service (remote control).
    
        Returns:
            None.
    
        """
        
        self.last_scorer = 20
        if self.last_goal_team == "atl":
            self.ask_legal(use_threads)
        elif self.last_goal_team == "mad":
            self.legal = True
            self.scorer_clicked(use_threads)

    def b_atl_10_clicked(self, obj, data=None, use_threads=True):
        """
        Description:
            This function takes action when this particular scorer is clicked
            in the select scorer screen. The variable last_scorer is set, the
            legal flag is set correspondingly and a function is called to 
            handle the score.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
            use_threads (boolean): This argument defines whether to use or not
            threads inside the function. Threads are not used
            when the call comes from the web service (remote control).
    
        Returns:
            None.
    
        """
        
        self.last_scorer = 21
        self.legal = True
        self.scorer_clicked(use_threads)
        
    def b_atl_11_clicked(self, obj, data=None, use_threads=True):
        """
        Description:
            This function takes action when this particular scorer is clicked
            in the select scorer screen. The variable last_scorer is set, the
            legal flag is set correspondingly and a function is called to 
            handle the score.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
            use_threads (boolean): This argument defines whether to use or not
            threads inside the function. Threads are not used
            when the call comes from the web service (remote control).
    
        Returns:
            None.
    
        """
        
        self.last_scorer = 22
        self.legal = True
        self.scorer_clicked(use_threads)
        
    def b_cross_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when the cross is clicked in the select
            scorer screen. It acts as a cancel and takes the user back to the
            match screen.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
                
        self.show_match_screen()
        
        return                                                              
    
    
    " *** Ask Scorer Screen - Functions *** "

    def scorer_clicked(self, use_threads=True):
        """
        Description:
            This function handles goals when a scorer has been clicked in 
            the select scorer screen. Depending on the legal flag and the last
            goal team, it determines where to add the goal. It stores the goal
            in the corresponding list, updates the match status and then goes 
            back to the match screen.

        Arguments:
            use_threads (boolean): This argument defines whether to use or not
            threads inside the function. Threads are not used
            when the call comes from the web service (remote control).
    
        Returns:
            None.
    
        """
        
        scorer_pid = self.selected_players[self.get_player_index(self.last_scorer)][0]
        scorer_nickname = self.selected_players_names[self.get_player_index(self.last_scorer)]
        
        " Guarra and Medio goals are considered not legal and last_goal_team is from the team responsible for the goal "
        " Own goals (en contra) are considered legal but last_goal_team is the team benefited from the goal (not the responsible) "
        " Still, in the DB, all these 3 types of goals are stored as illegal goals "
                                            
        if self.last_goal_team == "mad" and self.legal:            
            self.goals_mad.append([scorer_pid, self.last_goal_time, self.last_scorer, self.legal, scorer_nickname])
            self.score_mad += 1
            
        elif self.last_goal_team == "mad" and not self.legal:
            self.goals_atl.append([scorer_pid, self.last_goal_time, self.last_scorer, self.legal, scorer_nickname])
            self.score_atl += 1
            
        elif self.last_goal_team == "atl" and self.legal:
            self.goals_atl.append([scorer_pid, self.last_goal_time, self.last_scorer, self.legal, scorer_nickname])
            self.score_atl += 1
            
        elif self.last_goal_team == "atl" and not self.legal:
            self.goals_mad.append([scorer_pid, self.last_goal_time, self.last_scorer, self.legal, scorer_nickname])
            self.score_mad += 1                                                                                                                
                                
        # Check if match is over   
             
        self.update_match_status()
                                          
        # Show Match screen 
        
        if not use_threads: 
            Gdk.threads_enter()
        
            self.show_match_screen(use_threads=False)

            Gdk.threads_leave()
        
        else:
            self.show_match_screen()               
                                
        return
    
    def get_player_index(self, figure):
        """
        Description:
            This function returns the player number that is controlling
            a certain figure.

        Arguments:
            figure (Integer): Number of the figure
    
        Returns:
            None.
    
        """
        
        if figure in (1,2,3):
            return 0
        elif figure in (4,5,6,7,8,9,10,11):
            return 1
        elif figure in (12,13,14):
            return 2
        elif figure in (15,16,17,18,19,20,21,22):
            return 3
        
    def ask_legal(self, use_threads=True):
        """
        Description:
            This function sets a timeout and then shows the ask legal screen.

        Arguments:
            use_threads (boolean): This argument defines whether to use or not
            threads inside the function. Threads are not used
            when the call comes from the web service (remote control).
    
        Returns:
            None.
    
        """
        
        self.ask_timeout = 5
        
        self.show_ask_legal_screen(use_threads)                                            
            
        return

    
    " *** Ask Legal Screen - Buttons Handlers *** "
    
    def b_legal_yes_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when the yes button is clicked in the
            ask legal screen. It sets the flag to True and calls the function
            that handles the goals.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
        
        self.legal = True

        self.scorer_clicked()                
            
        return
    
    def b_legal_no_clicked(self, obj, data=None):
        """
        Description:
            This function takes action when the no button is clicked in the
            ask legal screen. It sets the flag to False and calls the function
            that handles the goals.

        Arguments:
            obj (Object): Widget from the Gtk GUI responsible for the callback.
            data: Extra data coming from the object that called the function.
    
        Returns:
            None.
    
        """
        
        self.legal = False
        
        self.scorer_clicked()
        
        return
    

    " *** Constructor *** "
    
    def __init__(self):
                
        if '169' in sys.argv:
            self.screen_ratio = 169
            self.gladefile = "futbolin_16_9.glade"
        else:            
            self.screen_ratio = 54
            self.gladefile = "futbolin_5_4.glade"
            
        self.builder = Gtk.Builder()
        self.builder.add_from_file(self.gladefile)
        self.builder.connect_signals(self) # Necessary ?
        self.window = self.builder.get_object("window1")
                        
        self.window.show()                        
                
        self.widgets_main = ["ebox_p1", "ebox_p2", "ebox_p3", "ebox_p4",
                             "bkg_p1", "bkg_p2", "bkg_p3", "bkg_p4",
                             "ebox_mad", "ebox_atl", "players_positions"]
                
        self.widgets_characters = ["ebox_b_start", "b_start_sg", "ebox_b_exit", "ebox_b_menu", "ch_line_1", "ch_line_2",
                                   "ebox_ch_1", "ebox_ch_2", "ebox_ch_3", "ebox_ch_4",
                                   "ebox_ch_5", "ebox_ch_6", "ebox_ch_7", "ebox_ch_8",
                                   "ebox_b_up", "ebox_b_down",
                                   "ebox_ch_9", "ebox_ch_10", "ebox_ch_11", "ebox_ch_12",
                                   "ebox_ch_13", "ebox_ch_14", "ebox_ch_15", "ebox_ch_16",
                                   "ebox_b_mundialito_1", "ebox_b_mundialito_2", "ebox_b_mundialito_3", "ebox_b_mundialito_4"]
        
        self.widgets_ask_confirmation = ["quit_question", "ebox_b_quit_yes", "ebox_b_quit_no"]    
                        
        self.widgets_match = ["ebox_score_mad", "scoreboard_sep", "ebox_score_atl", "timeboard", "webcam_snapshot",
                              "ebox_b_end_match", "ebox_b_cancel", "ebox_b_reset",
                              "goals_mad", "goals_atl", "goals_mad_txt", "goals_atl_txt", "stats_match_bkg",
                              "ball_1", "ball_2", "ball_3", "ball_4", "ball_5", "ball_6", "ball_7",
                              "bargraph_base", "bargraph_1", "bargraph_2", "bargraph_3", "bargraph_4", 
                              "bargraph_5", "bargraph_6", "bargraph_7", "bargraph_8",
                              "bargraph_9", "bargraph_10", "bargraph_11", "bargraph_12",
                              "stats_match_description", "stats_match_nickname", "stats_match_data", "mundialito_msg"]
                
        self.widgets_ask_scorer = ["field", "ebox_b_cross", "legal_timeout",
                                   "ebox_mad_1", "ebox_mad_2", "ebox_mad_3", "ebox_mad_4",
                                   "ebox_mad_5", "ebox_mad_6", "ebox_mad_7", "ebox_mad_8",
                                   "ebox_mad_9", "ebox_mad_10", "ebox_mad_11",
                                   "ebox_atl_1", "ebox_atl_2", "ebox_atl_3", "ebox_atl_4",
                                   "ebox_atl_5", "ebox_atl_6", "ebox_atl_7", "ebox_atl_8",
                                   "ebox_atl_9", "ebox_atl_10", "ebox_atl_11"]
                
        self.widgets_ask_legal = ["field_legal", "legal_timeout", "ebox_b_yes", "ebox_b_no"] 
        
        self.widgets_main_menu = ["main_menu_header", "ebox_b_championships", "ebox_b_stats", "ebox_b_seasons", "ebox_b_back_main_menu"]                               
        
        self.widgets_seasons = ["ebox_b_back_main_menu", "seasons_window", "seasons_headline", "seasons_main_bkg",
                                "ebox_b_seasons_left", "ebox_b_seasons_right", "seasons_winner", "seasons_losser"]
        
        self.widgets_championships = ["main_menu_header", "ebox_b_continue_championship", "ebox_b_new_championship", "ebox_b_back_main_menu"]
        
        self.widgets_new_championship = ["main_menu_header", "ebox_b_back_main_menu", "ebox_b_create_championship",
                                         "ebox_b_shuffle_matches", "ebox_b_shuffle_teams", "championship_teams_bkg",
                                         "ebox_b_team_1", "ebox_b_team_2", "ebox_b_team_3", "ebox_b_team_4",
                                         "ebox_b_team_5", "ebox_b_team_6", "ebox_b_team_7", "ebox_b_team_8"]
        
        self.widgets_select_team = ["main_menu_header", "ebox_b_back_main_menu", "ebox_b_accept", "ebox_b_up_2", "ebox_b_down_2",
                                    "bkg_team_p1", "bkg_team_p2", "ebox_team_p1", "ebox_team_p2", "ch_line_team", 
                                    "ebox_team_ch_1", "ebox_team_ch_2", "ebox_team_ch_3", "ebox_team_ch_4",
                                    "ebox_team_ch_5", "ebox_team_ch_6", "ebox_team_ch_7", "ebox_team_ch_8"]
        
        self.widgets_championship_matches = ["main_menu_header", "ebox_b_back_main_menu", "championship_matches_bkg",
                                             "ebox_b_m_team_1", "ebox_b_m_team_2", "ebox_b_m_team_3", "ebox_b_m_team_4", "ebox_b_m_team_5", "ebox_b_m_team_6", "ebox_b_m_team_7",
                                             "ebox_b_m_team_8", "ebox_b_m_team_9", "ebox_b_m_team_10", "ebox_b_m_team_11", "ebox_b_m_team_12", "ebox_b_m_team_13", "ebox_b_m_team_14"]
        
        self.widgets_championship_match = ["ebox_b_back_main_menu", "ebox_b_start", "b_start_sg", "goals_mad", "goals_atl"]
        
        self.widgets_congratulations = ["main_menu_header", "ebox_b_back_main_menu", "congratulations_bkg", "winner_1", "winner_2", "winner_1_txt", "winner_2_txt"]
        
        self.widgets_stats = ["main_menu_header", "ebox_b_back_main_menu", "stats_ch", "stats_ch_bkg", "ebox_b_stats_left", "ebox_b_stats_right", 
                              "stats_bkg", "stats_nickname", "stats_data_1", "stats_data_2", "stats_data_3", "stats_data_4", "stats_data_5"]
        
        
        # Change background if in DEV mode
        
        if 'dev' in sys.argv and '169' in sys.argv:                        
            self.background_image = "images/background_dev.jpg"
        elif 'dev' in sys.argv:
            self.background_image = "images/background_5_4_dev.jpg"
        elif '169' in sys.argv:
            self.background_image = "images/background.jpg"
        else:
            self.background_image = "images/background_5_4.jpg"    
                            
        self.builder.get_object("background").set_from_file(self.background_image)
        
        # Initialize Logging
        
        self.logger = self.initialize_logger()
        
        self.logger.info("\n" + "*" * 100)
        self.logger.info(" *** Initializing Futbolin *** ")
        self.logger.info(time.strftime("%H:%M:%S - %B %d %Y"))
                                        
        # Set the id name for widgets
                    
        self.set_widget_names()            
                                
        # Backup database once per day
        
        self.backup_database()
             
        # Calculate the order of the characters once per day
                                
        self.set_ch_order()
        
        self.n_characters = len(self.ch_order)
        
        self.last_row = (self.n_characters - 1) / 8 + 1                                       
                         
        self.current_row = 1
        self.ch_first_row  = []
        self.ch_second_row = []
                                
        self.in_match = False
        
        self.mundialito_mode = False
        
        # Show characters screen
                        
        self.show_characters_screen()
        
        # Initialize new game
                                
        self.initialize_game()
        
        return
    

" *** Remote Control Functions *** "

def run_remote_control_service(app, server, host, port, interval, reloader, quiet):
    """
    Description:
        This function handles the remote control web service. If it fails to launch
        it prints a message a returns. 

    Arguments:
        For the run() function of the bottle API.

    Returns:
        None.
    """
            
    try:
        bottle.run(app, server, host, port, interval, reloader, quiet)        
    except:
        print "Unable to start remote control web service."
    
    return

@bottle.route('/goal_mad', method='POST')
def set_goal_mad():
    """
    Description:
        This function handles the goals when a figure from Real Madrid is clicked
        within the remote control. If the application is not in a match, the function
        returns immediately. Depending on the information coming from the URL POST request,
        a different function is called. Since the function calls are done outside of the 
        GTK main thread, no threads can be used inside said functions. Therefore, the parameter
        use_threads is set to False.

    Arguments:
        None.

    Returns:
        None.
    """
    
    if not app.in_match:
        return
    
    scorer = int(bottle.request.forms.get('scorer'))
        
    scorers_functions = [app.b_mad_1_clicked,
                         app.b_mad_2_clicked,
                         app.b_mad_3_clicked,
                         app.b_mad_4_clicked,
                         app.b_mad_5_clicked,
                         app.b_mad_6_clicked,
                         app.b_mad_7_clicked,
                         app.b_mad_8_clicked,
                         app.b_mad_9_clicked,
                         app.b_mad_10_clicked,
                         app.b_mad_11_clicked]
        
    app.mad_goal(False)
        
    scorers_functions[scorer-1](None, None, False)
        
    return

@bottle.route('/goal_atl', method='POST')
def set_goal_atl():
    """
    Description:
        This function handles the goals when a figure from Atletico Madrid is clicked
        within the remote control. If the application is not in a match, the function
        returns immediately. Depending on the information coming from the URL POST request,
        a different function is called. Since the function calls are done outside of the 
        GTK main thread, no threads can be used inside said functions. Therefore, the parameter
        use_threads is set to False.

    Arguments:
        None.

    Returns:
        None.
    """
    
    if not app.in_match:
        return
    
    scorer = int(bottle.request.forms.get('scorer'))
        
    scorers_functions = [app.b_atl_1_clicked,
                         app.b_atl_2_clicked,
                         app.b_atl_3_clicked,
                         app.b_atl_4_clicked,
                         app.b_atl_5_clicked,
                         app.b_atl_6_clicked,
                         app.b_atl_7_clicked,
                         app.b_atl_8_clicked,
                         app.b_atl_9_clicked,
                         app.b_atl_10_clicked,
                         app.b_atl_11_clicked]
    
    app.atl_goal(False)
        
    scorers_functions[scorer-1](None, None, False)
    
    return
    
" *** Main *** "

if __name__ == "__main__":
    
    # Launch the listening service
    
    remote_control_ip = '10.7.2.25'
    
    bottle_thread = threading.Thread(target=run_remote_control_service, args=(None, 'wsgiref', remote_control_ip, 8080, 1, False, True,))        
    bottle_thread.daemon = True
    bottle_thread.start()
        
    # Launch the GUI
        
    GObject.threads_init() # Necessary?
    
    Gdk.threads_init()
    
    app = FutbolinGUI()

    # Start main GUI thread
    
    Gdk.threads_enter()
    Gtk.main()
    Gdk.threads_leave()
    
    
    
    
    
    
    
    
    

